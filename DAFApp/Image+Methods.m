//
//  Image+Methods.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 26/05/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import "Image+Methods.h"

@implementation Image (Methods)
-(UIImage*)getImage{
    NSLog(@"get image value %@",[[UIImage alloc]initWithData:[self image]]);
    return [[UIImage alloc]initWithData:[self image]];
}
@end
