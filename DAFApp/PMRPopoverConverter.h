//
//  PMRPopoverConverter.h
//  DAFApp
//
//  Created by Pruthvikar Reddy on 04/02/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMRPopoverConverter : UIViewController
@property (nonatomic, strong) Unit *unit;
@property (nonatomic) double value;
@property (nonatomic, strong) Unit *selectedUnit;
@property (nonatomic) double outputValue;
@property (nonatomic) int selectedIndex;
@end
