//
//  Image.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 03/06/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import "Image.h"
#import "Formula.h"


@implementation Image

@dynamic image;
@dynamic main;
@dynamic formulae;

@end
