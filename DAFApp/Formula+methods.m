//
//  Formula+methods.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 26/05/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import "Formula+methods.h"
#import "Image+Methods.h"
@implementation Formula (methods)
-(UIImage*)getImage
{
    for (Image* i in self.images) {
        if([i.main boolValue]){
            return [i getImage];
        }
    }
    return nil;
}

-(BOOL)hasImage
{
    for (Image* i in self.images) {
        if([i.main boolValue]){
            if (i.image) {
                return YES;
            }
        }
    }
    return NO;
}
@end
