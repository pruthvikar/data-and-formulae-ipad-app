//
//  UIMainScreenController.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 28/01/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import "PMRRootScreenController.h"
#import "PMRMenuViewController.h"
#import "PMRConnectionManager.h"
@interface PMRRootScreenController ()

@end

@implementation PMRRootScreenController

- (void)awakeFromNib {
	self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentController"];
	self.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"menuController"];
	self.backgroundImage = [self imageWithColor:[UIColor amethystColor]];
	self.delegate = (PMRMenuViewController *)self.leftMenuViewController;
    [self performSelector:@selector(showMenu) withObject:self afterDelay:0.005];
    }
-(void)showMenu
{
    [self presentLeftMenuViewController];

}


- (UIImage *)imageWithColor:(UIColor *)color {
	CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
	UIGraphicsBeginImageContext(rect.size);
	CGContextRef context = UIGraphicsGetCurrentContext();
    
	CGContextSetFillColorWithColor(context, [color CGColor]);
	CGContextFillRect(context, rect);
    
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
    
	return image;
}

@end
