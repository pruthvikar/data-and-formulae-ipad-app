//
//  main.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 21/10/2013.
//  Copyright (c) 2013 Pruthvikar Reddy. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PMRAppDelegate.h"

int main(int argc, char *argv[]) {
	@autoreleasepool {
		return UIApplicationMain(argc, argv, nil, NSStringFromClass([PMRAppDelegate class]));
	}
}
