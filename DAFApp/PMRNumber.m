//
//  PMRNumber.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 30/01/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import "PMRNumber.h"
#include "Complex.h"
#import "PMRFormulaTableCell.h"

@interface PMRNumber ()
@property (nonatomic, strong) NSNumber *real;
@property (nonatomic, strong) NSNumber *imaginary;
@end


@implementation PMRNumber

- (id)init {
	self = [super init];
	if (self) {
		self.real = [NSNumber numberWithFloat:0.0];
		self.imaginary = [NSNumber numberWithFloat:0.0];
	}
	return self;
}

- (void)setNumberWithReal:(NSNumber *)real andImaginary:(NSNumber *)imaginary {
	self.real = real;
	self.imaginary = imaginary;
}

- (int)countOccurencesOf:(NSString *)substring inString:(NSString *)string {
	int count = 0,
    length = (int)[string length];
	NSRange range = NSMakeRange(0, length);
	while (range.location != NSNotFound) {
		range = [string rangeOfString:substring options:0 range:range];
		if (range.location != NSNotFound) {
			range = NSMakeRange(range.location + range.length, length - (range.location + range.length));
			count++;
		}
	}
	return count;
}

- (BOOL)isValidString:(NSString *)string {
	NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789.i+-"];
    
	for (int i = 0; i < [string length]; i++) {
		unichar c = [string characterAtIndex:i];
        
		if (![myCharSet characterIsMember:c]) {
			return NO;
		}
	}
    
	return YES;
}

- (NSArray *)separated:(NSString *)str With:(NSString *)sep {
	NSArray *components = [str componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:sep]];
	NSLog(@"components%@", components);
	return components;
}

- (BOOL)setNumberWithCell:(PMRFormulaTableCell *)cell;
{
	self.imaginary = [NSNumber numberWithDouble:[cell.imag.text doubleValue]];
	cell.imag.text = self.imaginary.stringValue;
	self.real = [NSNumber numberWithDouble:[cell.real.text doubleValue]];
	cell.real.text = self.real.stringValue;
	return YES;
}

- (BOOL)setNumberWithString:(NSString *)value {
	NSString *real;
	NSString *imag;
	NSArray *poscomponents = [self separated:value With:@"+"];
	NSArray *negcomponents = [self separated:value With:@"-"];
	NSArray *icomponents = [self separated:value With:@"i"];
    
	if ([poscomponents count] == 1 && [negcomponents count] == 1) {
		if ([icomponents count] == 2 && [[icomponents objectAtIndex:1]isEqualToString:@""]) {
			imag = [icomponents objectAtIndex:0];
		}
	}
	if ([poscomponents count] == 2 || [negcomponents count] == 2) {
		if ([icomponents count] == 2 && [[icomponents objectAtIndex:1]isEqualToString:@""]) {
			imag = [icomponents objectAtIndex:0];
		}
	}
	self.imaginary = [NSNumber numberWithDouble:imag.doubleValue];
	self.real = [NSNumber numberWithDouble:real.doubleValue];
    
	return YES;
}

- (void)setNumberWithNumber:(NSNumber *)value {
	self.imaginary = @(0.0);
	self.real = value;
}

- (NSString *)stringValue {
	//    NSLog(self.imaginary);
	if (self.imaginary.doubleValue != 0) {
		if ([self.imaginary doubleValue] > 0) {
            if ([self.real doubleValue] > 0) {
            return [NSString stringWithFormat:@"+%@ +%@i", [self.real stringValue], [self.imaginary stringValue]];
            }
            else
            {
            return [NSString stringWithFormat:@"%@ +%@i", [self.real stringValue], [self.imaginary stringValue]];
            }

		}
		else {
            if ([self.real doubleValue] > 0) {
                return [NSString stringWithFormat:@"+%@ %@i", [self.real stringValue], [self.imaginary stringValue]];
            }
            else
            {
                return [NSString stringWithFormat:@"%@ %@i", [self.real stringValue], [self.imaginary stringValue]];
            }

		}
	}
	return [self.real stringValue];
}

- (NSString *)getReal {
	return [self.real stringValue];
}

- (NSString *)getImag {
	return [self.imaginary stringValue];
}

- (NSString *)getNumberAsString {
	//    NSLog(self.imaginary);
	if (self.imaginary.doubleValue != 0) {
		if (self.imaginary > 0) {
			return [NSString stringWithFormat:@"%@+%@*I", [self.real stringValue], [self.imaginary stringValue]];
		}
		else {
			return [NSString stringWithFormat:@"%@-%@*I", [self.real stringValue], [self.imaginary stringValue]];
		}
	}
	return [self.real stringValue];
}

@end
