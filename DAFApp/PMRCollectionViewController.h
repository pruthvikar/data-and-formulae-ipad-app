//
//  PMRViewController.h
//  DAFApp
//
//  Created by Pruthvikar Reddy on 21/10/2013.
//  Copyright (c) 2013 Pruthvikar Reddy. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HideKeyboardDelegate <NSObject>
// recipe == nil on cancel
- (void)hideKeyboard;
@end

@protocol formulaDelegate <NSObject>
// recipe == nil on cancel
- (void)selectedFormula:(Formula *)selectedFormula;
@end

@interface PMRCollectionViewController : UICollectionViewController <UICollectionViewDataSource, UICollectionViewDataSource, NSFetchedResultsControllerDelegate, UISearchBarDelegate>
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) Formula *selectedFormula;
@property (nonatomic, weak) id <HideKeyboardDelegate> delegate;
@property (nonatomic, weak) id <formulaDelegate> formulaDelegate;
@property (nonatomic, strong) NSString *mode;
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText andScopeIndex:(NSUInteger)scopeIndex;
//@property (nonatomic,strong) NSFetchedResultsController *fetchedResultsController;

@end
