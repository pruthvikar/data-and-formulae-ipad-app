//
//  PMRPopoverConverter.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 04/02/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import "PMRPopoverConverter.h"
#import "PMRUnitConverter.h"
@interface PMRPopoverConverter () <UIPickerViewDelegate, UIPickerViewDataSource>
@property (strong, nonatomic) IBOutlet UIPickerView *picker;
@property (strong, nonatomic) NSArray *allUnits;
@end

@implementation PMRPopoverConverter

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
    
	//    _picker=[[UIPickerView alloc] initWithFrame:self.view.frame];
    
	_picker.delegate = self;
	_picker.dataSource = self;
    
	_allUnits = [_unit.category.units sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]];
    
	_selectedUnit = _unit;
	_outputValue = _value;
	// Do any additional setup after loading the view.
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	return [_unit.category.units count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	NSString *display = [NSString stringWithFormat:@"%@ (%@)", [(Unit *)[_allUnits objectAtIndex:row] name], [(Unit *)[_allUnits objectAtIndex:row] display]];
	return [NSString stringWithFormat:@"%0.05g %@", [PMRUnitConverter convertValue:_value fromUnit:_unit toUnit:[_allUnits objectAtIndex:row]], display];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
	_selectedUnit = _allUnits[row];
	_outputValue = [PMRUnitConverter convertValue:_value fromUnit:_unit toUnit:_selectedUnit];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	NSInteger row = [_allUnits indexOfObject:_unit];
	[_picker selectRow:row inComponent:0 animated:NO];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
