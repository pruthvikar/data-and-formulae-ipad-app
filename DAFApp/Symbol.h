//
//  Symbol.h
//  DAFApp
//
//  Created by Pruthvikar Reddy on 03/06/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Meta.h"

@class Formula, UnitCategory;

@interface Symbol : Meta

@property (nonatomic, retain) NSNumber * constant;
@property (nonatomic, retain) NSString * display;
@property (nonatomic, retain) NSNumber * isConstant;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * onlyReal;
@property (nonatomic, retain) NSString * sympy;
@property (nonatomic, retain) NSSet *equation;
@property (nonatomic, retain) UnitCategory *unitCategory;
@end

@interface Symbol (CoreDataGeneratedAccessors)

- (void)addEquationObject:(Formula *)value;
- (void)removeEquationObject:(Formula *)value;
- (void)addEquation:(NSSet *)values;
- (void)removeEquation:(NSSet *)values;

@end
