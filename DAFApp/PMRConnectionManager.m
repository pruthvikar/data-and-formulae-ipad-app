//
//  PMRConnectionManager.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 04/11/2013.
//  Copyright (c) 2013 Pruthvikar Reddy. All rights reserved.
//

//const NSString* kBaseUrl=@"http://192.168.1.4/";
//const NSString* kBaseUrl=@"http://162.13.95.57/";
NSString *const kBaseUrl = @"http://daf.pruth.co/";
//NSString* const kBaseUrl=@"http://127.0.0.1:8000/";

#import "PMRConnectionManager.h"
#import "AFNetworking.h"
#import "Formula+methods.h"
#import "MBProgressHUD.h"

@interface PMRConnectionManager ()
@property (strong, nonatomic) NSOperationQueue *queue;
@property (weak,nonatomic) PMRAppDelegate* ad;
@end

@implementation PMRConnectionManager

+ (id)sharedManager {
	static PMRConnectionManager *sharedMyManager = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
	    sharedMyManager = [[self alloc] init];
	});
	return sharedMyManager;
}

- (id)init {
	self = [super initWithBaseURL:[NSURL URLWithString:kBaseUrl]];
	if (self) {
		self.responseSerializer = [AFCompoundResponseSerializer compoundSerializerWithResponseSerializers:@[[AFJSONResponseSerializer serializer], [AFImageResponseSerializer serializer]]];
		[self.operationQueue setMaxConcurrentOperationCount:1];
        _ad = (PMRAppDelegate *)[[UIApplication sharedApplication] delegate];
	}
	return self;
}

- (void)syncWithDB:(id)refresh {
    NSLog(@"Synchronizing data");
	NSArray *formulae = [self getAllEntitiesOfType:@"Formula"];
	NSArray *Units = [self getAllEntitiesOfType:@"Unit"];
	NSArray *Symbols = [self getAllEntitiesOfType:@"Symbol"];
	NSArray *UnitCategories = [self getAllEntitiesOfType:@"UnitCategory"];
    NSArray *Images = [self getAllEntitiesOfType:@"Image"];
	NSMutableDictionary *timeStampsFormulae = [NSMutableDictionary dictionary];
	NSMutableDictionary *timeStampsUnits = [NSMutableDictionary dictionary];
	NSMutableDictionary *timeStampsUnitCategories = [NSMutableDictionary dictionary];
	NSMutableDictionary *timeStampsSymbols = [NSMutableDictionary dictionary];
    NSMutableDictionary *timeStampsImages = [NSMutableDictionary dictionary];
	for (Formula *f in formulae) {
		[timeStampsFormulae setObject:f.lastModified forKey:f.pk];
	}
	for (Unit *u in Units) {
		[timeStampsUnits setObject:u.lastModified forKey:u.pk];
	}
	for (UnitCategory *uc in UnitCategories) {
		[timeStampsUnitCategories setObject:uc.lastModified forKey:uc.pk];
	}
	for (Symbol *s in Symbols) {
		[timeStampsSymbols setObject:s.lastModified forKey:s.pk];
	}
    for (Image *i in Images) {
		[timeStampsImages setObject:i.lastModified forKey:i.pk];
	}
    
	NSDictionary *params = @{ @"Formulae":timeStampsFormulae,
		                      @"Units":timeStampsUnits,
		                      @"UnitCategories":timeStampsUnitCategories,
		                      @"Symbols":timeStampsSymbols,
                              @"Images":timeStampsImages};
	[self POST:@"sync" parameters:params success: ^(AFHTTPRequestOperation *operation, id responseObject) {
	    [self processUpdates:responseObject];
        
	    if ([refresh isKindOfClass:[MBProgressHUD class]]) {
            [refresh hide:YES];
        }
        else if ([refresh isKindOfClass:[UIRefreshControl class]])
        {
        [refresh endRefreshing];
        }
	} failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
	    NSLog(@"Error: %@", error);
	    if ([refresh isKindOfClass:[MBProgressHUD class]]) {
            [refresh hide:YES];
        }
        else if ([refresh isKindOfClass:[UIRefreshControl class]])
        {
            [refresh endRefreshing];
        }
	}];
}


- (void)processUpdates:(NSDictionary *)dict {

    
    NSArray *images = [dict objectForKey:@"Images"];
    
	if (images) {
		for (NSDictionary *i in images) {
			Image *editImage = [self getOrCreate:@"Image" WithID:i[@"pk"]];
			if ([[i objectForKey:@"delete"] boolValue]) {
				[_ad.managedObjectContext deleteObject:editImage];
			}
			else {
				editImage.lastModified = [NSDate date];
                editImage.main=i[@"main"];
                [self downloadImage:i[@"image"] to:editImage];
			}
		}
	}

    
	NSArray *formulae = [dict objectForKey:@"Formulae"];
	if (formulae) {
		for (NSDictionary *f in formulae) {
			Formula *editFormula = [self getOrCreate:@"Formula" WithID:[f objectForKey:@"pk"]];
			NSLog(@"%@", [f objectForKey:@"delete"]);
			if ([[f objectForKey:@"delete"] boolValue]) {
				[_ad.managedObjectContext deleteObject:editFormula];
			}
			else {
				editFormula.lastModified = [NSDate date];
				editFormula.category = f[@"category"];
				editFormula.name = f[@"name"];
				editFormula.symbols = f[@"sympyVars"];
				editFormula.summary = f[@"summary"];
				editFormula.legal = f[@"legal"];
				NSMutableSet *symbolsSet = [NSMutableSet set];
				for (NSNumber *s in f[@"symbols"]) {
					[symbolsSet addObject:[self getOrCreate:@"Symbol" WithID:s]];
				}
				[editFormula setSymbols:symbolsSet];
                NSMutableSet *imagesSet = [NSMutableSet set];
				for (NSNumber *i in f[@"images"]) {
					[imagesSet addObject:[self getOrCreate:@"Image" WithID:i]];
				}
				[editFormula setImages:imagesSet];
                editFormula.hasMain=@([editFormula hasImage]);
			}
		}
	}
    
	NSArray *symbols = [dict objectForKey:@"Symbols"];
	if (symbols) {
		for (NSDictionary *s in symbols) {
			Symbol *editSymbol = [self getOrCreate:@"Symbol" WithID:[s objectForKey:@"pk"]];
            
			if ([[s objectForKey:@"delete"] boolValue]) {
				[_ad.managedObjectContext deleteObject:editSymbol];
			}
			else {
				editSymbol.lastModified = [NSDate date];
                editSymbol.name=s[@"name"];
				editSymbol.onlyReal = s[@"onlyReal"];
				editSymbol.sympy = s[@"sympy"];
				editSymbol.unitCategory = [self getOrCreate:@"UnitCategory" WithID:s[@"unit"]];
                editSymbol.isConstant = s[@"isConstant"];
				editSymbol.constant = s[@"constant"];
                editSymbol.display=s[@"display"];
			}
		}
	}
    
	NSArray *unitCategories = [dict objectForKey:@"UnitCategories"];
    
	if (unitCategories) {
		for (NSDictionary *uc in unitCategories) {
			UnitCategory *editCategory = [self getOrCreate:@"UnitCategory" WithID:uc[@"pk"]];
            
			if ([[uc objectForKey:@"delete"] boolValue]) {
				[_ad.managedObjectContext deleteObject:editCategory];
			}
			else {
				editCategory.name = uc[@"name"];
				editCategory.isDimensionless = uc[@"isDimensionless"];
				editCategory.lastModified = [NSDate date];
				NSMutableSet *unitsSet = [NSMutableSet set];
				for (NSNumber *u in uc[@"units"]) {
					[unitsSet addObject:[self getOrCreate:@"Unit" WithID:u]];
				}
				NSMutableSet *symbolsSet = [NSMutableSet set];
				for (NSNumber *s in uc[@"symbols"]) {
					[symbolsSet addObject:[self getOrCreate:@"Symbol" WithID:s]];
				}
				[editCategory setSymbols:symbolsSet];
				[editCategory setUnits:unitsSet];
			}
		}
	}
    
	NSArray *units = [dict objectForKey:@"Units"];
    
	if (units) {
		for (NSDictionary *u in units) {
			Unit *editUnit = [self getOrCreate:@"Unit" WithID:u[@"pk"]];
			if ([[u objectForKey:@"delete"] boolValue]) {
				[_ad.managedObjectContext deleteObject:editUnit];
			}
			else {
				editUnit.lastModified = [NSDate date];
				editUnit.name = u[@"name"];
				editUnit.multiplicand = u[@"multiplicand"];
				editUnit.isBaseUnit = u[@"isBaseUnit"];
				editUnit.addend = u[@"addend"];
				editUnit.display = u[@"units"];
			}
		}
	}
    
    
	[_ad saveContext];
}

- (void)solveEquation:(NSDictionary *)variables success:(ConnectionBlock)ConnectionBlock {
	MBProgressHUD *waitingHUD =[self getHUD];
	waitingHUD.labelText = @"Solving Equation";
	[self POST:@"solveEquation" parameters:variables success: ^(AFHTTPRequestOperation *operation, id responseObject) {
	    //        NSString* solution=[[responseObject objectForKey:@"answers"] objectAtIndex:1];
	    ConnectionBlock(responseObject);
	    NSLog(@"%@", responseObject);
	    [waitingHUD hide:YES];
	} failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
	    NSLog(@"Error: %@", error);
	    [waitingHUD hide:YES];
	}];
}

- (void)getSteamValuesFor:(NSDictionary *)variables success:(ConnectionBlock)ConnectionBlock {
	MBProgressHUD *waitingHUD =[self getHUD];	waitingHUD.labelText = @"Getting Steam Values";
	[self POST:@"getSteamValues" parameters:variables success: ^(AFHTTPRequestOperation *operation, id responseObject) {
	    ConnectionBlock(responseObject);
	    NSLog(@"%@", responseObject);
	    [waitingHUD hide:YES];
	} failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
	    NSLog(@"Error: %@", error);
	    [waitingHUD hide:YES];
	}];
}

-(MBProgressHUD*)getHUD
{
	UIWindow *window = [[[UIApplication sharedApplication] windows] lastObject];
	MBProgressHUD *waitingHUD = [MBProgressHUD showHUDAddedTo:window animated:YES];

    waitingHUD.color=[UIColor wetAsphaltColor];
    return waitingHUD;
}

- (void)sendFeedback:(NSString *)feedback success:(ConnectionBlock)ConnectionBlock failure:(FailureBlock)FailureBlock
{

	MBProgressHUD *waitingHUD =[self getHUD];
	waitingHUD.labelText = @"Sending Feedback";
    
    [self POST:@"feedback" parameters:@{@"summary":feedback} success: ^(AFHTTPRequestOperation *operation, id responseObject) {
	    ConnectionBlock(responseObject);
	} failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
	    FailureBlock();
	}];

}

- (id)getOrCreate:(NSString *)type WithID:(NSNumber *)pk {
	/*No profile exists so we have to create one*/

	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:type inManagedObjectContext:_ad.managedObjectContext];
	[request setEntity:entity];
	[request setFetchLimit:1];
	NSPredicate *pred = [NSPredicate predicateWithFormat:@"pk = %@", pk];
	[request setPredicate:pred];
	NSError *error = nil;
	NSArray *result = [_ad.managedObjectContext executeFetchRequest:request error:&error];
    
	if (error) {
		/*Handle Error*/
		NSLog(@"error");
	}
	else if (![result count]) {
		Meta *obj = [NSEntityDescription insertNewObjectForEntityForName:type inManagedObjectContext:_ad.managedObjectContext];
        
		//assign default values to new instance of profile
		obj.lastModified = [NSDate date];
		obj.pk = pk;
        
		return obj;
	}
    
    
	return [result firstObject];
}

- (NSArray *)getAllEntitiesOfType:(NSString *)type {

	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:type inManagedObjectContext:_ad.managedObjectContext];
	[request setEntity:entity];
	NSError *error = nil;
	NSArray *results = [_ad.managedObjectContext executeFetchRequest:request error:&error];
    
	if (error) {
		/*Handle Error*/
		NSLog(@"error");
	}
    
	return results;
}

- (void)downloadImage:(NSString *)identifier to:(Image *)i {

    
	[self GET:identifier parameters:Nil success: ^(AFHTTPRequestOperation *operation, id responseObject) {
	    i.image = UIImagePNGRepresentation(responseObject);
        NSLog(@"image downloaded");
        i.formulae.hasMain=@([i.formulae hasImage]);
        [_ad saveContext];

	} failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
	    NSLog(@"error: %@",  operation.responseString);
	}];
}


@end
