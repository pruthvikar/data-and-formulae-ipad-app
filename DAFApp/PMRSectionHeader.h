//
//  PMRSectionHeader.h
//  DAFApp
//
//  Created by Pruthvikar Reddy on 05/12/2013.
//  Copyright (c) 2013 Pruthvikar Reddy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMRSectionHeader : UICollectionReusableView
@property (strong, nonatomic) UILabel *sectionName;

@end
