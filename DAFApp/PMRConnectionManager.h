//
//  PMRConnectionManager.h
//  DAFApp
//
//  Created by Pruthvikar Reddy on 04/11/2013.
//  Copyright (c) 2013 Pruthvikar Reddy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"

typedef void (^ConnectionBlock)(NSDictionary *);
typedef void (^FailureBlock)();

@interface PMRConnectionManager : AFHTTPRequestOperationManager



+ (id)sharedManager;
- (void)syncWithDB:(id)refresh;
- (void)solveEquation:(NSDictionary *)variables success:(ConnectionBlock)ConnectionBlock;
- (void)getSteamValuesFor:(NSDictionary *)variables success:(ConnectionBlock)ConnectionBlock;
- (void)sendFeedback:(NSString *)feedback success:(ConnectionBlock)ConnectionBlock failure:(FailureBlock)FailureBlock;
@end
