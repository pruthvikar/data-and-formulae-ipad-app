//
//  Formula+methods.h
//  DAFApp
//
//  Created by Pruthvikar Reddy on 26/05/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import "Formula.h"

@interface Formula (methods)
-(UIImage*)getImage;
-(BOOL)hasImage;
@end
