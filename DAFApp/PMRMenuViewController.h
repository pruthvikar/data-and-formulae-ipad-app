//
//  PMRMenuViewController.h
//  DAFApp
//
//  Created by Pruthvikar Reddy on 13/03/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"
@interface PMRMenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, RESideMenuDelegate>

@property (strong, readwrite, nonatomic) UITableView *tableView;

@end
