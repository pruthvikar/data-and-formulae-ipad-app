//
//  Unit.h
//  DAFApp
//
//  Created by Pruthvikar Reddy on 03/06/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Meta.h"

@class UnitCategory;

@interface Unit : Meta

@property (nonatomic, retain) NSNumber * addend;
@property (nonatomic, retain) NSNumber * isBaseUnit;
@property (nonatomic, retain) NSNumber * multiplicand;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * display;
@property (nonatomic, retain) UnitCategory *category;

@end
