//
//  PMRFormulaViewController.h
//  DAFApp
//
//  Created by Pruthvikar Reddy on 23/10/2013.
//  Copyright (c) 2013 Pruthvikar Reddy. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Formula;

@interface PMRFormulaViewController : UIViewController

@property (strong, nonatomic) Formula *formula;

@end
