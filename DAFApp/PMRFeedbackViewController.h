//
//  PMRFeedbackViewController.h
//  DAFApp
//
//  Created by Pruthvikar Reddy on 08/11/2013.
//  Copyright (c) 2013 Pruthvikar Reddy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMRFeedbackViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextView *comments;

@end
