//
//  PMRUnitConverter.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 14/11/2013.
//  Copyright (c) 2013 Pruthvikar Reddy. All rights reserved.
//

#import "PMRUnitConverter.h"

@implementation PMRUnitConverter

+ (double)convertValue:(double)val fromUnit:(Unit *)a toUnit:(Unit *)b {
	double temp = [self toBase:a value:val];
	return [self fromBase:b value:temp];
}

+ (double)fromBase:(Unit *)unit value:(double)value {
	return value *[unit.multiplicand doubleValue] + [[unit addend] doubleValue];
}

+ (double)toBase:(Unit *)unit value:(double)value {
	return (value - [[unit addend] doubleValue]) / [unit.multiplicand doubleValue];
}

@end
