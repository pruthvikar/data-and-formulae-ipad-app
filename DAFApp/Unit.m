//
//  Unit.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 03/06/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import "Unit.h"
#import "UnitCategory.h"


@implementation Unit

@dynamic addend;
@dynamic isBaseUnit;
@dynamic multiplicand;
@dynamic name;
@dynamic display;
@dynamic category;

@end
