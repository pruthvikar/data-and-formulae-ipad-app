//
//  PMRResistorViewController.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 01/02/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import "PMRResistorViewController.h"
#import "RESideMenu.h"
#define kBlack  @"Black"
#define kBrown  @"Brown"
#define kRed    @"Red"
#define kOrange @"Orange"
#define kYellow @"Yellow"
#define kGreen  @"Green"
#define kBlue   @"Blue"
#define kViolet @"Violet"
#define kGray   @"Gray"
#define kWhite  @"White"
#define kGold   @"Gold"
#define kSilver @"Silver"
#define kNone   @"Cream"

@interface PMRResistorViewController ()
@property (nonatomic, strong) NSArray *components;
@property (nonatomic, strong) NSArray *a, *b, *c, *d, *e, *t;
@end

@implementation PMRResistorViewController

#pragma mark -
#pragma mark TODO  mega ohms, removal of decimals for results, incorporate strings into tolerance, null the unapplicable colors, page for info, switch statement for choosing,
#pragma mark -
#pragma mark Instance Variables



@synthesize resultLabel;


#pragma mark-
#pragma mark Custom Methods

// A method to change colors of the views according to the picker view changed component
- (IBAction)showMenu:(id)sender {
	[self.sideMenuViewController presentLeftMenuViewController];
}

- (UIColor *)getColor:(NSString *)color {
	UIColor *viewColor;
	if ([color isEqual:kYellow])
		viewColor = [UIColor yellowColor];
	else if ([color isEqual:kBlue])
		viewColor = [UIColor blueColor];
	else if ([color isEqual:kBlack])
		viewColor = [UIColor blackColor];
	else if ([color isEqual:kGray])
		viewColor = [UIColor grayColor];
	else if ([color isEqual:kGreen])
		viewColor = [UIColor greenColor];
	else if ([color isEqual:kRed])
		viewColor = [UIColor redColor];
	else if ([color isEqual:kBrown])
		viewColor = [UIColor brownColor];
	else if ([color isEqual:kWhite])
		viewColor = [UIColor whiteColor];
	else if ([color isEqual:kViolet])
		viewColor = [UIColor colorWithRed:148.0 / 255.0 green:0.0 blue:211.0 / 255.0 alpha:1.0];
	else if ([color isEqual:kOrange])
		viewColor = [UIColor orangeColor];
	else if ([color isEqual:kGold])
		viewColor = [UIColor colorWithRed:252.0 / 255.0 green:194.0 / 255.0 blue:0 alpha:1.0];
	else if ([color isEqual:kSilver])
		viewColor = [UIColor colorWithRed:192.0 / 255.0 green:192.0 / 255.0 blue:192.0 / 255.0 alpha:1.0];
	else if ([color isEqual:kNone])
		viewColor = [UIColor colorWithRed:220.0 / 255.0 green:220.0 / 255.0 blue:180.0 / 255.0 alpha:1.0];
    
	return viewColor;
}

#pragma mark -
#pragma mark Init requirements for pickerView instance


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return [_components count];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	return [[_components objectAtIndex:component] count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	return nil;
}

- (void)initPickerValues {
	// results are stored in variables
    
	// band colors.  values are dynamically deduced from the position in the array
    
    
	_a = @[kBlack,
	       kBrown,
	       kRed,
	       kOrange,
	       kYellow,
	       kGreen,
	       kBlue,
	       kViolet,
	       kGray,
	       kWhite];
	_b = @[kBlack,
	       kBrown,
	       kRed,
	       kOrange,
	       kYellow,
	       kGreen,
	       kBlue,
	       kViolet,
	       kGray,
	       kWhite];
	_c = @[kSilver,
	       kGold,
	       kBlack,
	       kBrown,
	       kRed,
	       kOrange,
	       kYellow,
	       kGreen,
	       kBlue,
	       kViolet,
	       kGray,
	       kWhite];
	_d = @[kNone,
	       kBlack,
	       kBrown,
	       kRed,
	       kOrange,
	       kYellow,
	       kGreen,
	       kBlue,
	       kViolet,
	       kGray,
	       kWhite];
	_e = @[
           kGray,
           kViolet,
           kBlue,
           kGreen,
           kBrown,
           kRed,
           kWhite,
           kNone];
	_components = @[_a, _b, _c, _d, _e];
    
    
    
    
	// tolerance values
	_t = @[@0.05,
	       @.1,
	       @.25,
	       @0.5,
	       @1,
	       @2,
	       @5,
	       @10,
	       @20
           ];
}

- (void)calculateUpdate {
	if ([_picker selectedRowInComponent:3] == 0) {
		[self fourBandCalc];
	}
	else {
		[self fiveBandCalc];
	}
}

- (NSString *)getStringForResistance:(float)resistance andTolerance:(float)tolerance {
	NSString *unitR = @"";
	//    NSString* unitT=@"";
	if ((int)(log10(resistance)) < 0) {
		resistance *= 1000;
		tolerance *= 1000;
		unitR = @"m";
	}
	else if ((int)(log10(resistance)) < 3) {
	}
	else if ((int)(log10(resistance)) < 6) {
		resistance /= 1000;
		tolerance /= 1000;
		unitR = @"k";
	}
    
	else if ((int)(log10(resistance)) < 9) {
		resistance /= 1000000;
		tolerance /= 1000000;
		unitR = @"M";
	}
    
	else {
		resistance /= 1000000000;
		tolerance /= 1000000000;
		unitR = @"G";
	}
    
	return [NSString stringWithFormat:@"%@ %@Ω ± %@ %@Ω", [[NSNumber numberWithFloat:resistance] stringValue], unitR, [[NSNumber numberWithFloat:tolerance] stringValue], unitR];
}

- (void)fourBandCalc {
	float resistance = ([_picker selectedRowInComponent:0] * 10 + [_picker selectedRowInComponent:1]) * pow(10, [_picker selectedRowInComponent:2] - 2);
	float tolerance = resistance * ([[_t objectAtIndex:[_picker selectedRowInComponent:4]] floatValue] / 100.0);
	NSLog(@"%f", [[_t objectAtIndex:4] floatValue]);
	self.resultLabel.text = [self getStringForResistance:resistance andTolerance:tolerance];
}

- (void)fiveBandCalc {
	float resistance = ([_picker selectedRowInComponent:0] * 100 + [_picker selectedRowInComponent:1] * 10 + [_picker selectedRowInComponent:2]) * pow(10, [_picker selectedRowInComponent:3] - 2);
	float tolerance = resistance * ([[_t objectAtIndex:[_picker selectedRowInComponent:4]] floatValue] / 100.0);
	NSLog(@"%f", [[_t objectAtIndex:4] floatValue]);
	self.resultLabel.text = [self getStringForResistance:resistance andTolerance:tolerance];
}

#pragma mark -
#pragma mark Update Resistor Values based on user input

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
	if (component == 2 && (row == 0 || row == 1)) {
		[pickerView selectRow:0 inComponent:3 animated:YES];
	}
	else if (component == 3 && ([pickerView selectedRowInComponent:2] == 0 || [pickerView selectedRowInComponent:2] == 1)) {
		[pickerView selectRow:2 inComponent:2 animated:YES];
	}
    
	[self calculateUpdate]; // calculate resistance and update GUI
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
	return 150;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
	UIColor *bgColor = [self getColor:[[_components objectAtIndex:component] objectAtIndex:row]];
	NSString *imageName = [@[@"left", @"middle", @"middle", @"tolerance", @"right"] objectAtIndex : component];
	UIImage *image = [UIImage imageNamed:imageName];
    
	UIImageView *myImageView = [[UIImageView alloc] initWithImage:image];
    
	myImageView.backgroundColor = bgColor;
	myImageView.contentMode = UIViewContentModeCenter;
	return myImageView;
}

#pragma mark -
#pragma mark Ordinary App Stuff

- (void)viewDidLoad {
	[self initPickerValues];  // method for populating picker values
	[super viewDidLoad];
	self.title = @"Resistor Calculator";
	self.picker.delegate = self;
	[self calculateUpdate];

	self.resultLabel.textColor = [UIColor asbestosColor];
	self.navigationController.navigationBar.barTintColor = [UIColor concreteColor];
    
    
	self.navigationController.navigationBar.translucent = NO;
	self.view.backgroundColor = [UIColor cloudsColor];
    
    
	//Sets topic image based on device orientation
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}

@end
