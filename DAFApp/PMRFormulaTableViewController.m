//
//  PMRFormulaTableViewController.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 04/11/2013.
//  Copyright (c) 2013 Pruthvikar Reddy. All rights reserved.
//

#import "PMRFormulaTableViewController.h"
#import "PMRFormulaTableCell.h"
#import "PMRConnectionManager.h"
#import "PMRNumber.h"
#import "PMRPopoverConverter.h"
#import "PMRFormulaTableConvertCell.h"
#import "PMREntity.h"
#import "PMRUnitConverter.h"
#import "UIPopoverController+FlatUI.h"
#import "UnitCategory+Methods.h"
@interface PMRFormulaTableViewController () <UIPopoverControllerDelegate>
@property (strong, nonatomic) NSArray *entities;
//@property (strong,nonatomic) NSMutableArray* values;
//@property (strong,nonatomic) NSMutableArray* units;
//@property (weak,nonatomic) UILabel* ans;
@property (weak, nonatomic) UILabel *ans;
@property (strong, nonatomic) UIPopoverController *pc;
@property (nonatomic) BOOL showOther;
@property (nonatomic, strong) UIStoryboardPopoverSegue *settingsPopoverSegue;

@property (strong,nonatomic) NSArray* otherResults;

@end

@implementation PMRFormulaTableViewController

- (id)initWithStyle:(UITableViewStyle)style {
	self = [super initWithStyle:style];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (IBAction)lockClicked:(id)sender {
	UIButton *button = (UIButton *)sender;
	PMRFormulaTableConvertCell *cell = (PMRFormulaTableConvertCell *)button.superview.superview.superview;
    
	NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
	for (int i = 0; i < [_entities count]; i++) {
		PMRFormulaTableConvertCell *cellOther = (PMRFormulaTableConvertCell *)[[self tableView] cellForRowAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0]];
		if (![[[_entities[i] symbol] isConstant] boolValue]) {
			[[cellOther solve] setTitle:@"Set Unknown" forState:UIControlStateNormal];
			[[cellOther real] setEnabled:YES];
			[[cellOther convert] setEnabled:YES];
		}
		[_entities[i] setLockedValue:YES];
	}
	[[cell solve] setTitle:@"" forState:UIControlStateNormal];
	[[cell real] setEnabled:NO];
	[[cell convert] setEnabled:NO];
	[_entities[indexPath.row] setLockedValue:NO];
}

- (IBAction)solveClicked:(id)sender {
	[self solveEquation];
}

- (void)convertClicked:(id)sender {
	CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
	UIButton *button = (UIButton *)sender;
	PMRFormulaTableConvertCell *cell = (PMRFormulaTableConvertCell *)button.superview.superview.superview;
    
    
	UIStoryboard *mystoryboard = [UIStoryboard storyboardWithName:@"iPad" bundle:nil];
    
	//    [self performSegueWithIdentifier:@"converter" sender:self];
	PMRPopoverConverter *pc  = [mystoryboard instantiateViewControllerWithIdentifier:@"popoverConverter"];
	pc.value = cell.real.text.doubleValue;
	pc.unit = [_entities[indexPath.row] unit];
	pc.selectedIndex = (int)indexPath.row;
	pc.preferredContentSize = CGSizeMake(500, 216);
	UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:pc];
	[popover configureFlatPopoverWithBackgroundColor:[UIColor midnightBlueColor] cornerRadius:3];
    
	popover.delegate = self;
	// Store the popover in a custom property for later use.
	popover.popoverContentSize = CGSizeMake(500, 216);
	self.pc = popover;
	CGRect popoverFrame = [self.parentViewController.view convertRect:cell.units.bounds fromView:button];
    
	//    popoverFrame.size=CGSizeMake(1, 1);
	//    popoverFrame.origin=[self touch]
	NSLog(@"%@", NSStringFromCGRect(popoverFrame));
	[self.pc presentPopoverFromRect:popoverFrame inView:self.parentViewController.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
	//        [pc setModalInPopover:YES];
	//        [self setModalInPopover:YES];
	//    [self presentViewController:pc animated:YES completion:nil];
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
	PMRPopoverConverter *controller =  (PMRPopoverConverter *)popoverController.contentViewController;
	double newValue = controller.outputValue;
	Unit *newUnit = controller.selectedUnit;
	int index = controller.selectedIndex;
	PMREntity *ent = _entities[index];
	[ent setUnit:newUnit];
    
	//    [self storeTextFieldValues];
	//    [[ent value] setNumberWithNumber:[NSNumber numberWithDouble:newValue]];
	ent.real = newValue;
	[self.tableView reloadData];
	//Do something with data
}

- (void)solveEquation {
	//    if (![self storeTextFieldValues]) {
	//        return;
	//    }
	PMRConnectionManager *manager = [PMRConnectionManager sharedManager];
    
    __block BOOL locked=false;
	NSMutableDictionary *variables = [NSMutableDictionary dictionary];
	[_entities enumerateObjectsUsingBlock: ^(PMREntity *obj, NSUInteger idx, BOOL *stop) {
	    if (obj.lockedValue) {
            locked=true;
	        [variables setObject:[obj sympyValue] forKey:obj.symbol.sympy];
		}
	    else {
	        [variables setObject:[NSNull null] forKey:obj.symbol.sympy];
		}
	}];
    if (!locked){
    self.otherResults=@[@"Select a value to solve for"];
    [self.tableView reloadData];
        _showOther=false;
        return;
    }
    
	[variables setObject:_formula.pk forKey:@"formulaID"];

    
	NSLog(@"%@", variables);
	[manager solveEquation:variables success: ^(NSDictionary *response) {
	    //        PMRNumber* solutionNumber=[_values objectAtIndex:variableIndex.row];
	    NSLog(@"%@", response);
        if ([response objectForKey:@"error"]) {
            self.otherResults=@[@"No solutions found"];
            _showOther=false;
            [self.tableView reloadData];
        }
	    else if ([[response objectForKey:@"number of solutions"]integerValue] != 0) {
                    _showOther=true;
	        NSNumber *real = response[@"answers"][@"0"][@"real"];
	        NSNumber *imag = response[@"answers"][@"0"][@"imaginary"];
	        NSString *vars = response[@"answers"][@"0"][@"vars"];
	        for (PMREntity * e in _entities) {
	            if ([e.symbol.sympy isEqualToString:vars]) {
	                if (!e.unit) {
	                    e.real = real.doubleValue;
	                    e.imag = imag.doubleValue;
					}
	                else if (![e.unit.category.isDimensionless boolValue]) {
	                    e.real = [PMRUnitConverter fromBase:e.unit value:real.doubleValue];
	                    e.imag = imag.doubleValue;
					}
				}
			}
	        [[self tableView] reloadData];
	        for (PMREntity * e in _entities) {
	            NSLog(@"values are %f", e.real);
			}
            
	        if ([[response objectForKey:@"number of solutions"]integerValue] > 1) {
	            PMRNumber *tempNumber = [[PMRNumber alloc] init];

	            NSMutableArray *solutionStrings = [[NSMutableArray alloc] init];
                
	            NSInteger numOfSolutions = [[response objectForKey:@"number of solutions"]integerValue];
	            for (int i = 1; i < numOfSolutions; i++) {
	                NSDictionary *answer = [response[@"answers"] objectForKey:[NSString stringWithFormat:@"%d", i]];
	                NSNumber *real = [NSNumber numberWithDouble:[[answer objectForKey:@"real"] doubleValue]];
	                NSNumber *imag = [NSNumber numberWithDouble:[[answer objectForKey:@"imaginary"] doubleValue]];
                    
	                [tempNumber setNumberWithReal:real andImaginary:imag];
	                [solutionStrings addObject:[tempNumber stringValue]];
				}
                self.otherResults=solutionStrings;
                            [self.tableView reloadData];

			}
	        else {
                self.otherResults=@[];
                _showOther=false;
                [self.tableView reloadData];
			}
		}
	    else {
	        NSLog(@"TOO MANY SOLUTIONS");
		}
	}];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 45; // some value
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if ([self.formula.symbols count]==0) {
        return nil;
    }
	UIView *emptyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.contentSize.width, 100)];
	UILabel *solutionsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.contentSize.width, 100)];
	solutionsLabel.backgroundColor = [UIColor cloudsColor];
	emptyView.backgroundColor = [UIColor whiteColor];
	[self.tableView setTableFooterView:emptyView];
	[emptyView addSubview:solutionsLabel];
	solutionsLabel.textAlignment = NSTextAlignmentCenter;
	solutionsLabel.numberOfLines = 3;
	self.ans = solutionsLabel;
	emptyView.backgroundColor = [UIColor cloudsColor];
	return emptyView;
}

- (void)viewDidLoad {
	[super viewDidLoad];
    if ([self.formula.symbols count]==0) {
        return;
    }
	NSMutableArray *tempArray = [NSMutableArray array];
	self.tableView.backgroundColor = [UIColor cloudsColor];
	self.view.backgroundColor = [UIColor cloudsColor];
    
	NSArray *vars = [_formula.symbols sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]];
	for (int i = 0; i < [_formula.symbols count]; i++) {
		PMREntity *ent = [PMREntity entity];
		[tempArray addObject:ent];
		Symbol *s = vars[i];
		ent.symbol = s;
		if ([s.unitCategory.isDimensionless boolValue]) {
			ent.unit = nil;
		}
		else {
			ent.unit = [s.unitCategory getBaseUnit];
		}
	}
    
	_entities = [tempArray copy];
}


- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	// Return the number of sections.
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// Return the number of rows in the section.
	return [_entities count] + [_otherResults count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.row == [_entities count]) {
		static NSString *cellID = @"SolveCell";
		UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
		cell.backgroundColor = [UIColor cloudsColor];
        
		return cell;
	}
    
    
    
    if (indexPath.row > [_entities count]) {

		UITableViewCell *cell = [[UITableViewCell alloc] init];
        NSLog(@"%@",NSStringFromCGRect(cell.frame));
		cell.backgroundColor = [UIColor cloudsColor];

        UITextView* tv=[[UITextView alloc]initWithFrame:cell.frame];
        tv.backgroundColor=[UIColor clearColor];
        [tv setFont:[UIFont systemFontOfSize:15.0]];
        [cell addSubview:tv];
        tv.textColor=[UIColor asbestosColor];
        tv.textAlignment=NSTextAlignmentRight;
        tv.text=[_otherResults objectAtIndex:indexPath.row-_entities.count-1];
        
        if (indexPath.row == [_entities count]+1 && _showOther)
        {
            UITextView* ot=[[UITextView alloc]initWithFrame:cell.frame];
            ot.backgroundColor=[UIColor clearColor];
            [cell addSubview:ot];
            ot.textColor=[UIColor asbestosColor];
            ot.textAlignment=NSTextAlignmentLeft;
            [ot setFont:[UIFont systemFontOfSize:15.0]];
            ot.text=@"Solutions";
        
        }
		return cell;
	}
    
    
    
    
    
	Symbol *sym = [_entities[indexPath.item] symbol];
	PMREntity *ent = _entities[indexPath.item];
    
    
	static NSString *CellIdentifierReal = @"RealCell";
	PMRFormulaTableConvertCell *cell = (PMRFormulaTableConvertCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifierReal forIndexPath:indexPath];
    
	cell.backgroundColor = [UIColor cloudsColor];
	[_entities[indexPath.row] setRealValue:[sym.onlyReal boolValue]];
	if ([sym.isConstant boolValue]) {
		ent.real = sym.constant.doubleValue;
		[cell.real setText:[ent displayValue]];
		cell.convert.hidden = YES;
		[cell.real setEnabled:NO];
		cell.solve.hidden = YES;
	}
	else {
		if (![sym.onlyReal boolValue]) {
			[cell.real addTarget:self action:@selector(complexTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
			if ([cell.real.text isEqualToString:@""]) {
				cell.real.text = [self getValueFromText:@"0" andIndexPath:indexPath];
			}
			else {
				cell.real.text = [_entities[indexPath.row] displayValue];
			}
		}
		else {
			[cell.real addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            
			if ([cell.real.text isEqualToString:@""]) {
				cell.real.text = [self getRealValueFromText:@"0" andIndexPath:indexPath];
			}
            
			else {           cell.real.text = [_entities[indexPath.row] displayValue]; }
		}
	}
	if (![ent unit]) {
		cell.units.hidden = YES;
		cell.convert.hidden = YES;
	}
	else {
		cell.units.text = [ent.unit display];
	}
	cell.name.text = [sym display];
	NSLog(@"%f", ent.real);
	if ([ent.unit.category.units count] <= 1) {
		cell.convert.hidden = YES;
	}
	[cell.convert addTarget:self action:@selector(convertClicked:) forControlEvents:UIControlEventTouchUpInside];
	cell.real.delegate = self;
	cell.real.tag = indexPath.row;
	cell.real.textColor = [UIColor asbestosColor];
	cell.solve.tintColor = [UIColor amethystColor];
	cell.convert.tintColor = [UIColor amethystColor];
	cell.name.textColor = [UIColor asbestosColor];
	cell.real.backgroundColor = [UIColor cloudsColor];
    
	return cell;
}

- (int)countOccurencesOf:(NSString *)substring inString:(NSString *)string {
	int count = 0, length = (int)[string length];
	NSRange range = NSMakeRange(0, length);
	while (range.location != NSNotFound) {
		range = [string rangeOfString:substring options:0 range:range];
		if (range.location != NSNotFound) {
			range = NSMakeRange(range.location + range.length, length - (range.location + range.length));
			count++;
		}
	}
	return count;
}

- (NSString *)getRealValueFromText:(NSString *)text andIndexPath:(NSIndexPath *)indexPath {
	NSLog(@"%@", text);
	NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
	[nf setMinimumFractionDigits:1];
	[nf setMaximumFractionDigits:10];
	[nf setMinimumIntegerDigits:1];
	[nf setPositivePrefix:[nf plusSign]];
	[nf setNegativePrefix:[nf minusSign]];
    
	[(PMREntity *)_entities[indexPath.row] setReal :[text doubleValue]];
	[(PMREntity *)_entities[indexPath.row] setImag : 0.0];
	//    NSLog(@"%@",text);
	return [nf stringFromNumber:[NSNumber numberWithDouble:[text doubleValue]]];
}

- (NSString *)getValueFromText:(NSString *)text andIndexPath:(NSIndexPath *)indexPath {
	NSLog(@"original %@", text);
	NSString *a = @"([-]?\\d+\\.?\\d*|[-]?\\d*\\.?\\d+)\\s*";
	// ... possibly following that with whitespace
	//    NSString* b =@"\\s*";
	// ... followed by a plus
    
	// Match any other float, and save it
	NSString *c = @"[+]?([-]?\\d+\\.?\\d*|[-]?\\d*\\.?\\d+)[i]?";
	// ... followed by 'i'
	//    NSString* d =@"[i]?";
    
	NSString *regexString = [NSString stringWithFormat:@"%@%@", a, c];
    
	//        NSString* regex=@"\\d+";
	NSError *error;
	NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexString options:0 error:&error];
	NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
	[nf setMinimumFractionDigits:1];
	[nf setMaximumFractionDigits:10];
	[nf setMinimumIntegerDigits:1];
	[nf setPositivePrefix:[nf plusSign]];
	[nf setNegativePrefix:[nf minusSign]];
	NSArray *matches = [regex matchesInString:text options:0 range:NSMakeRange(0, [text length])];
    
	for (NSTextCheckingResult *result in matches) {
		NSNumber *real = [NSNumber numberWithDouble:[[text substringWithRange:[result rangeAtIndex:1]] doubleValue]];
		NSNumber *imag = [NSNumber numberWithDouble:[[text substringWithRange:[result rangeAtIndex:2]] doubleValue]];
		//        if ([imag length]==0) {
		//            imag=@"0";
		//        }
		NSLog(@"real %f", [real doubleValue]);
		NSLog(@"imag %f", [imag doubleValue]);
		[(PMREntity *)_entities[indexPath.row] setReal :[real doubleValue]];
		[(PMREntity *)_entities[indexPath.row] setImag :[imag doubleValue]];
		for (int i = 0; i < [result numberOfRanges]; i++) {
			NSLog(@"matches %@", [text substringWithRange:[result rangeAtIndex:i]]);
		}
        
		return [NSString stringWithFormat:@"%@ %@i", [nf stringFromNumber:real], [nf stringFromNumber:imag]];
	}
	return @"+0.0 +0.0i";
}

- (void)textFieldDidChange:(UITextField *)sender {
	CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
	UITextRange *selectedRange = sender.selectedTextRange;
    
	NSString *cleanedString = [[[sender.text componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+.i"] invertedSet]] componentsJoinedByString:@""]stringByReplacingOccurrencesOfString:@"+" withString:@" "];
    
	//    [self getValueFromText:cleanedString];
	sender.text = [self getRealValueFromText:cleanedString andIndexPath:indexPath];
    
	[sender setSelectedTextRange:selectedRange];
}

- (void)complexTextFieldDidChange:(UITextField *)sender {
	CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
	UITextRange *selectedRange = sender.selectedTextRange;
    
	NSString *cleanedString = [[[sender.text componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+.i"] invertedSet]] componentsJoinedByString:@""]stringByReplacingOccurrencesOfString:@"+" withString:@" "];
    
	sender.text = [self getValueFromText:cleanedString andIndexPath:indexPath];
    


	[sender setSelectedTextRange:selectedRange];
}

@end
