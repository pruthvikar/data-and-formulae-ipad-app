//
//  UnitCategory+Methods.h
//  DAFApp
//
//  Created by Pruthvikar Reddy on 25/05/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import "UnitCategory.h"
@class Unit;
@interface UnitCategory (Methods)
-(Unit*)getBaseUnit;
@end
