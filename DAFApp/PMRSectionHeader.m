//
//  PMRSectionHeader.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 05/12/2013.
//  Copyright (c) 2013 Pruthvikar Reddy. All rights reserved.
//

#import "PMRSectionHeader.h"

@implementation PMRSectionHeader

- (id)initWithFrame:(CGRect)frame {
	if (IS_IPAD) {
		frame = CGRectMake(0.0, 0.0, 768.0, 75.0);
	}
	else {
		frame = CGRectMake(0.0, 0.0, 320.0, 45.0);
	}
    
	self = [super initWithFrame:frame];
	if (self) {
        //        self.backgroundColor=[UIColor blackColor];
		self.sectionName = [[UILabel alloc]initWithFrame:frame];
		self.sectionName.textAlignment = NSTextAlignmentCenter;
		[self addSubview:self.sectionName];
		self.sectionName.textColor = [UIColor whiteColor];
		if (IS_IPAD) {
			[self.sectionName setFont:[UIFont systemFontOfSize:60.0]];
		}
		else {
			[self.sectionName setFont:[UIFont systemFontOfSize:30.0]];
		}
		self.backgroundColor = [UIColor concreteColor];
        //        UIToolbar* bgToolbar = [[UIToolbar alloc] initWithFrame:frame];
        //        bgToolbar.barStyle = UIBarStyleDefault;
        //        [self addSubview:bgToolbar];
        //        [self sendSubviewToBack:bgToolbar];
        //        self.sectionName.backgroundColor=[UIColor blackColor];
        
		// Initialization code
	}
	return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
