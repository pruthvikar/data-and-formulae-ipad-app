//
//  PMRFormulaTableCell.h
//  DAFApp
//
//  Created by Pruthvikar Reddy on 04/11/2013.
//  Copyright (c) 2013 Pruthvikar Reddy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMRFormulaTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UITextField *real;
@property (strong, nonatomic) IBOutlet UITextField *imag;
@property (nonatomic, strong) IBOutlet UILabel *name;

@end
