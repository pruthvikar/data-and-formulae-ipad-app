//
//  PMRCollectionViewCell.h
//  DAFApp
//
//  Created by Pruthvikar Reddy on 21/10/2013.
//  Copyright (c) 2013 Pruthvikar Reddy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMRCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UIImageView *equation;


@end
