//
//  PMRUnitConverter.h
//  DAFApp
//
//  Created by Pruthvikar Reddy on 14/11/2013.
//  Copyright (c) 2013 Pruthvikar Reddy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PMRUnitConverter : NSObject
+ (double)convertValue:(double)val fromUnit:(Unit *)a toUnit:(Unit *)b;
+ (double)fromBase:(Unit *)unit value:(double)value;
+ (double)toBase:(Unit *)unit value:(double)value;
@end
