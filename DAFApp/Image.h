//
//  Image.h
//  DAFApp
//
//  Created by Pruthvikar Reddy on 03/06/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Meta.h"

@class Formula;

@interface Image : Meta

@property (nonatomic, retain) NSData * image;
@property (nonatomic, retain) NSNumber * main;
@property (nonatomic, retain) Formula *formulae;

@end
