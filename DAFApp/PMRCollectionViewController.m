//
//  PMRViewController.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 21/10/2013.
//  Copyright (c) 2013 Pruthvikar Reddy. All rights reserved.
//




#import "PMRCollectionViewController.h"
#import "PMRCollectionViewCell.h"
#import "UIImage+PMRRecolor.h"
#import "Formula.h"
#import "Formula+methods.h"
#import "PMRFormulaViewController.h"
#import "PMRConnectionManager.h"
#import "Image+Methods.h"
#import "PMRSectionHeader.h"
#import "PMRViewController.h"
#import "PMRDragViewController.h"
@interface PMRCollectionViewController ()
@property (strong, nonatomic) NSArray *forms;
@property (strong, nonatomic) NSArray *results;
@property (strong, nonatomic) PMRConnectionManager *connectionManager;
@property (strong, nonatomic) UIRefreshControl *refresh;
@end

@implementation PMRCollectionViewController
{
	NSMutableArray *_objectChanges;
	NSMutableArray *_sectionChanges;
}


@synthesize fetchedResultsController = _fetchedResultsController;



- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
	return UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0);
}

- (void)viewDidUnload {
	self.fetchedResultsController = nil;
}

- (NSFetchedResultsController *)fetchedResultsController {
	if (_fetchedResultsController) {
		return _fetchedResultsController;
	}
    
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
	NSEntityDescription *entity = [NSEntityDescription
	                               entityForName:@"Formula" inManagedObjectContext:_managedObjectContext];
    
	[fetchRequest setEntity:entity];
    NSPredicate* pred=[NSPredicate predicateWithFormat:@"hasMain==True"];
    [fetchRequest setPredicate:pred];
    
    
    
	//    [fetchRequest setFetchBatchSize:50];
    
    
	NSSortDescriptor *sortCategory = [[NSSortDescriptor alloc] initWithKey:@"category" ascending:YES];
	// The second sort descriptor sorts the items within each section:
	NSSortDescriptor *sortName = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	NSArray *sortDescriptors = @[sortCategory, sortName];
	[fetchRequest setSortDescriptors:sortDescriptors];
    
	NSFetchedResultsController *theFetchedResultsController = [[NSFetchedResultsController alloc]
	                                                           initWithFetchRequest:fetchRequest
	                                                           managedObjectContext:self.managedObjectContext
                                                               sectionNameKeyPath:@"category"
                                                               cacheName:nil];
    
    
    
    
	self.fetchedResultsController = theFetchedResultsController;
	_fetchedResultsController.delegate = self;
    
	return _fetchedResultsController;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(NSIndexPath *)sender {
	if ([segue.identifier isEqualToString:@"viewFormula"]) {
		PMRFormulaViewController *vc = [segue destinationViewController];
		vc.formula = [self.fetchedResultsController objectAtIndexPath:sender];
	}
}

- (void)viewDidLoad {
	PMRAppDelegate *appDelegate = (PMRAppDelegate *)[[UIApplication sharedApplication] delegate];
	_managedObjectContext = appDelegate.managedObjectContext;
	[super viewDidLoad];
	self.collectionView.backgroundColor = [UIColor cloudsColor];
	_connectionManager = [PMRConnectionManager sharedManager];
	//

	[self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"othercell"];
	_refresh = [[UIRefreshControl alloc] init];
	_refresh.tintColor = [UIColor grayColor];
	[_refresh addTarget:self action:@selector(syncWithDB) forControlEvents:UIControlEventValueChanged];
	[self.collectionView addSubview:_refresh];
   	self.collectionView.alwaysBounceVertical = YES;
	[self.collectionView reloadData];
    

	[self.collectionView registerClass:[PMRSectionHeader class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"SectionHeader"];
	NSError *error;
	if (![[self fetchedResultsController] performFetch:&error]) {
		// Update to handle the error appropriately.
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		exit(-1);  // Fail
	}

	_objectChanges = [NSMutableArray array];
	_sectionChanges = [NSMutableArray array];

}

- (void)syncWithDB {
	[_connectionManager syncWithDB:_refresh];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {

	[self.collectionView reloadData];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
	NSMutableDictionary *change = [NSMutableDictionary new];
	switch (type) {
		case NSFetchedResultsChangeInsert:
			change[@(type)] = newIndexPath;
			break;
            
		case NSFetchedResultsChangeDelete:
			change[@(type)] = indexPath;
			break;
            
		case NSFetchedResultsChangeUpdate:
			change[@(type)] = indexPath;
			break;
            
		case NSFetchedResultsChangeMove:
			change[@(type)] = @[indexPath, newIndexPath];
			break;
	}
	[_objectChanges addObject:change];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
	NSMutableDictionary *change = [NSMutableDictionary new];
    
	switch (type) {
		case NSFetchedResultsChangeInsert:
			change[@(type)] = @(sectionIndex);
			break;
            
		case NSFetchedResultsChangeDelete:
			change[@(type)] = @(sectionIndex);
			break;
	}
    
	[_sectionChanges addObject:change];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
	// The fetch controller has sent all current change notifications, so tell the table view to process all updates.
	[self.collectionView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText andScopeIndex:(NSUInteger)scopeIndex {
	NSArray *searchProperties = @[@"category", @"name", @"summary"];
	if (![searchText isEqualToString:@""] || scopeIndex != 0) {
		NSArray *terms = [searchText componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
		NSMutableArray *subpredicates = [NSMutableArray array];
        
		for (NSString *term in terms) {
			if ([term length] == 0) {
				continue;
			}
            
			NSMutableArray *propertyPredicates = [NSMutableArray array];
            
			for (NSString *property in searchProperties) {
				[propertyPredicates addObject:[NSPredicate predicateWithFormat:@"%K contains[cd] %@", property, term]];
			}
            
			NSPredicate *propertyFilter = [NSCompoundPredicate orPredicateWithSubpredicates:propertyPredicates];
			[subpredicates addObject:propertyFilter];
		}
        
		if (scopeIndex != 0) {
			[subpredicates addObject:[NSPredicate predicateWithFormat:@"category contains[cd] %@", [searchBar.scopeButtonTitles objectAtIndex:scopeIndex]]];
		}
        
		NSPredicate *filter = [NSCompoundPredicate andPredicateWithSubpredicates:subpredicates];
		[self.fetchedResultsController.fetchRequest setPredicate:filter];
	}
	else {
		NSPredicate *predicate = Nil;
		[self.fetchedResultsController.fetchRequest setPredicate:predicate];
	}
    
	NSError *error = nil;
	if (![[self fetchedResultsController] performFetch:&error]) {
		// Handle error
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		exit(-1);  // Fail
	}
	[self.collectionView reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
    
	return;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
	id sectionInfo =
    [[self.fetchedResultsController sections] objectAtIndex:section];
    
	return [sectionInfo numberOfObjects];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
	NSInteger count = [[_fetchedResultsController sections] count];
    
	return count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
	if ([[(PMRViewController *)self.parentViewController mode] isEqualToString:@"selection"]) {
		self.formulaDelegate = [(PMRViewController *)self.parentViewController delegate];
        
		[self.formulaDelegate selectedFormula:[self.fetchedResultsController objectAtIndexPath:indexPath]];
		[self dismissViewControllerAnimated:YES completion:nil];
	}
	else {
		[self performSegueWithIdentifier:@"viewFormula" sender:indexPath];
	}
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
	PMRSectionHeader *sh = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"SectionHeader" forIndexPath:indexPath];

	sh.sectionName.text = [[[_fetchedResultsController sections] objectAtIndex:indexPath.section] name];
	return sh;
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	[self.delegate hideKeyboard];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
	if (IS_IPAD) {
		return CGSizeMake(768.0, 75.0);
	}
	return CGSizeMake(320.0, 45.0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.section == [[_fetchedResultsController sections] count]) {
		if (IS_IPAD) {
			return CGSizeMake(700.0, 100.0);
		}
		else {
			return CGSizeMake(300.0, 100.0);
		}
	}
	float scale;
	if (IS_IPAD) {
		scale = 0.5;
	}
	else {
		scale = 0.3;
	}
	Formula *info = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
	UIImage *image = [info getImage];
	return CGSizeMake(ceilf((image.size.width * scale) + 40.0), ceilf((image.size.height * scale) + 80.0));
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
	PMRCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"DAFCell" forIndexPath:indexPath];
	Formula *info = [self.fetchedResultsController objectAtIndexPath:indexPath];
	cell.backgroundColor = [UIColor whiteColor];
	UIImage *eqn = [info getImage];

    
	[cell.equation setImage:[eqn colorAnImage:[UIColor whiteColor]]];
	cell.name.textColor = [UIColor whiteColor];
	cell.backgroundColor = [UIColor turquoiseColor];
	[cell.name setText:info.name];
    
	return cell;
}



@end
