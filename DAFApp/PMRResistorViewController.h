//
//  PMRResistorViewController.h
//  DAFApp
//
//  Created by Pruthvikar Reddy on 01/02/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface PMRResistorViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;


@end
