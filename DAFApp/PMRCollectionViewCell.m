//
//  PMRCollectionViewCell.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 21/10/2013.
//  Copyright (c) 2013 Pruthvikar Reddy. All rights reserved.
//

#import "PMRCollectionViewCell.h"

@implementation PMRCollectionViewCell

- (id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if (self) {
		//        [self.contentView addSubview:self.name];
		//        self.alpha=0.5f;
		// Initialization code
	}
	return self;
}

- (void)prepareForReuse {
	self.backgroundColor = [UIColor redColor];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
	CALayer *layer = [self layer];
	[layer setMasksToBounds:YES];
	[layer setCornerRadius:15.0];
	[layer setRasterizationScale:[[UIScreen mainScreen] scale]];
	[layer setShouldRasterize:YES];
    
	[layer setShadowColor:[[UIColor blackColor] CGColor]];
	[layer setShadowOffset:CGSizeMake(2.0f, 2.0f)];
	[layer setShadowRadius:1.0f];
	[layer setShadowOpacity:0.9f];
	//
	//
	[layer setShadowPath:[[UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:layer.cornerRadius] CGPath]];
}

@end
