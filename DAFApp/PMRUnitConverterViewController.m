//
//  PMRUnitConverterViewController.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 03/02/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import "PMRUnitConverterViewController.h"
#import "PMRConnectionManager.h"
#import "Unit.h"
#import "PMRUnitConverter.h"
#import "PMRPopoverConverter.h"
#import "RESideMenu.h"
#import "PMRConnectionManager.h"
#import "MBProgressHUD.h"
@interface PMRUnitConverterViewController () <NSFetchedResultsControllerDelegate, UIPickerViewDelegate>
@property (strong, nonatomic) IBOutlet UIPickerView *type;
@property (strong, nonatomic) IBOutlet UIPickerView *from;

@property (strong, nonatomic) NSFetchedResultsController *units;
@property (strong, nonatomic) NSFetchedResultsController *categories;
@property (strong, nonatomic) IBOutlet UITextField *input;
@property (strong, nonatomic) IBOutlet UILabel *output;
@property (strong, nonatomic) IBOutlet UIButton *popover;
@property (strong, nonatomic) IBOutlet UIButton *swap;
@property (strong, nonatomic) NSString *prevValue;

@end

@implementation PMRUnitConverterViewController
- (IBAction)showMenu:(id)sender {
	[self.sideMenuViewController presentLeftMenuViewController];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (NSFetchedResultsController *)units {
	if (_units) {
		return _units;
	}
	PMRAppDelegate *appDelegate = (PMRAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
	NSEntityDescription *entity = [NSEntityDescription
	                               entityForName:@"Unit" inManagedObjectContext:appDelegate.managedObjectContext];
    
	[fetchRequest setEntity:entity];
	if ([[_categories fetchedObjects] count] > 0) {
		NSPredicate *pred = [NSPredicate predicateWithFormat:@"self.category.name = %@", [[[_categories fetchedObjects] objectAtIndex:0] name]];
		//    [fetchRequest setFetchBatchSize:50];
		[fetchRequest setPredicate:pred];
		//    [fetchRequest setFetchBatchSize:50];
	}
    
	//    NSSortDescriptor *sortCategory = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	// The second sort descriptor sorts the items within each section:
	NSSortDescriptor *sortName = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	//    NSArray *sortDescriptors = @[sortCategory, sortName];
	//    [fetchRequest setSortDescriptors:sortDescriptors];
	[fetchRequest setSortDescriptors:@[sortName]];
	NSFetchedResultsController *theFetchedResultsController = [[NSFetchedResultsController alloc]
	                                                           initWithFetchRequest:fetchRequest
	                                                           managedObjectContext:appDelegate.managedObjectContext
                                                               sectionNameKeyPath:@"category"
                                                               cacheName:nil];
	self.units = theFetchedResultsController;
	_units.delegate = self;
	return _units;
}

- (IBAction)swap:(UIButton *)sender {
	_input.text = [[NSNumber numberWithDouble:_output.text.doubleValue] stringValue];
	NSInteger tempIndex = [_from selectedRowInComponent:0];
	[_from selectRow:[_from selectedRowInComponent:1] inComponent:0 animated:YES];
	[_from selectRow:tempIndex inComponent:1 animated:YES];
	_output.text = [self calculateOutputForPicker:nil];
}

- (NSFetchedResultsController *)categories {
	if (_categories) {
		return _categories;
	}
	PMRAppDelegate *appDelegate = (PMRAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
	NSEntityDescription *entity = [NSEntityDescription
	                               entityForName:@"UnitCategory" inManagedObjectContext:appDelegate.managedObjectContext];
    
	[fetchRequest setEntity:entity];
	NSPredicate *pred = [NSPredicate predicateWithFormat:@"units.@count >= 1"];
    
	//    //    [fetchRequest setFetchBatchSize:50];
	[fetchRequest setPredicate:pred];
    
	//    NSSortDescriptor *sortCategory = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	// The second sort descriptor sorts the items within each section:
	NSSortDescriptor *sortName = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	//    NSArray *sortDescriptors = @[sortCategory, sortName];
	//    [fetchRequest setSortDescriptors:sortDescriptors];
	[fetchRequest setSortDescriptors:@[sortName]];
	NSFetchedResultsController *theFetchedResultsController = [[NSFetchedResultsController alloc]
	                                                           initWithFetchRequest:fetchRequest
	                                                           managedObjectContext:appDelegate.managedObjectContext
                                                               sectionNameKeyPath:nil
                                                               cacheName:nil];
	self.categories = theFetchedResultsController;
	_categories.delegate = self;
	return _categories;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	if ([pickerView isEqual:_type]) {
		return 1;
	}
    
	return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	if ([pickerView isEqual:_type]) {
		return [[_categories fetchedObjects]count];
	}
    
	return [[_units fetchedObjects] count];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
	UILabel *tView = (UILabel *)view;
	if (!tView) {
		tView = [[UILabel alloc] init];
	}
	tView.minimumScaleFactor = 0.5;
	tView.textAlignment = NSTextAlignmentCenter;
	tView.textColor = [UIColor asbestosColor];
	tView.text = [self pickerView:pickerView titleForRow:row forComponent:component];
	return tView;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	NSString *units;
	if ([pickerView isEqual:_type]) {
		return [(UnitCategory *)[[_categories fetchedObjects] objectAtIndex:row] name];
	}
	else if (component == 1) {
		units = [NSString stringWithFormat:@"%@ (%@)", [(Unit *)[[_units fetchedObjects] objectAtIndex:row] name], [(Unit *)[[_units fetchedObjects] objectAtIndex:row] display]];
		return [NSString stringWithFormat:@"%@ %@", [self calculateOutputForPicker:@(row)], units];
	}
	units = [NSString stringWithFormat:@"%@ (%@)", [(Unit *)[[_units fetchedObjects] objectAtIndex:row] name], [(Unit *)[[_units fetchedObjects] objectAtIndex:row] display]];
	return units;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
	if ([pickerView isEqual:_type]) {
        if ([[_categories fetchedObjects] count]==0) {
            return;
        }
		NSFetchRequest *fr = [_units fetchRequest];
		NSPredicate *pred = [NSPredicate predicateWithFormat:@"self.category.name = %@", [[[_categories fetchedObjects] objectAtIndex:row] name]];
		//    [fetchRequest setFetchBatchSize:50];
		[fr setPredicate:pred];
        
		NSError *error;
		if (![[self units] performFetch:&error]) {
			// Update to handle the error appropriately.
			NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
			exit(-1);  // Fail
		}
        
        
		_input.text = @"0.0";
	}
	[_from reloadAllComponents];
	_output.text = [self calculateOutputForPicker:Nil];
}

- (NSString *)calculateOutputForPicker:(NSNumber *)picker {
	if ([_from numberOfRowsInComponent:0] == 0) {
		return @"-";
	}
	NSInteger fromIndex = [_from selectedRowInComponent:0];
	NSInteger toIndex;
	if (picker) {
		toIndex = [picker integerValue];
	}
	else {
		toIndex = [_from selectedRowInComponent:1];
	}
    
    
	Unit *fromUnit = [[_units fetchedObjects]objectAtIndex:fromIndex];
	Unit *toUnit = [[_units fetchedObjects]objectAtIndex:toIndex];
	double ans = [PMRUnitConverter convertValue:_input.text.doubleValue fromUnit:fromUnit toUnit:toUnit];
	return [NSString stringWithFormat:@"%0.5g", ans];
}


- (void)textFieldDidChange:(UITextField *)sender {
	UITextRange *selectedRange = sender.selectedTextRange;
    
    NSScanner *scanner = [NSScanner scannerWithString:sender.text];
    BOOL isNumeric = [scanner scanDouble:NULL] && [scanner isAtEnd];
	if (!isNumeric) {
        sender.text=_prevValue;
    }
    else{
    _prevValue=sender.text;
    }
    [sender setSelectedTextRange:selectedRange];
	_output.text = [self calculateOutputForPicker:nil];
	[_from reloadAllComponents];
}

- (void)viewDidLoad {
	[super viewDidLoad];
	self.popover.hidden = YES;
	self.swap.tintColor = [UIColor amethystColor];
	self.input.textColor = [UIColor asbestosColor];
	self.type.delegate = self;
	self.from.delegate = self;
    self.input.keyboardType=UIKeyboardTypeDecimalPad;
    
    
	self.navigationController.navigationBar.barTintColor = [UIColor concreteColor];
    
    
	self.navigationController.navigationBar.translucent = NO;
	self.view.backgroundColor = [UIColor cloudsColor];
	self.output.hidden = YES;
    
    

    
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(syncWithDB)];
    
    
    


    
    
    
	[_input addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
	NSError *error;
	if (![[self categories] performFetch:&error]) {
		// Update to handle the error appropriately.
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		exit(-1);  // Fail
	}
    
	if (![[self units] performFetch:&error]) {
		// Update to handle the error appropriately.
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		exit(-1);  // Fail
	}
    
    
	// Do any additional setup after loading the view.
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
	// The fetch controller has sent all current change notifications, so tell the table view to process all updates.
	[self.type reloadAllComponents];
    	[self.from reloadAllComponents];
    [self pickerView:self.type didSelectRow:[self.type selectedRowInComponent:0] inComponent:0];
}



- (void)syncWithDB {
    MBProgressHUD* HUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.color=[UIColor wetAsphaltColor];
    HUD.labelText=@"Synchronizing";
    [[PMRConnectionManager sharedManager] syncWithDB:HUD];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	PMRPopoverConverter *dvc = segue.destinationViewController;
	dvc.unit = [[_units fetchedObjects] objectAtIndex:0];
	dvc.value = _input.text.doubleValue;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
