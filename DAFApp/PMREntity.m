//
//  PMREntity.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 06/02/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import "PMREntity.h"
#import "PMRUnitConverter.h"
#import "PMRFormulaTableCell.h"
#import "PMRNumber.h"
#import "PMRFormulaTableConvertCell.h"
@implementation PMREntity
+ (instancetype)entity {
	return [[self alloc] init];
}

- (id)init {
	// Call superclass's initializer
	self = [super init];
	if (!self) {
		return nil;
	}
	//    _value=[[PMRNumber alloc] init];
    
	return self;
}

//-(BOOL)setNumberWithCell:(id)cell
//{
//
//    if ([cell isKindOfClass:[PMRFormulaTableCell class]]) {
//        _val =[cell real].text.doubleValue;
////        [_value setNumberWithReal:[NSNumber numberWithDouble:[cell real].text.doubleValue] andImaginary:[NSNumber numberWithDouble:[cell imag].text.doubleValue]];
//        return YES;
//    }
//    else{
//        _val =[cell real].text.doubleValue;
////        [_value setNumberWithNumber:[NSNumber numberWithDouble:[cell real].text.doubleValue]];
//        return YES;
//
//    }
//
//}

- (NSString *)displayValue {
	if (!_realValue) {
		NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
		[nf setMinimumFractionDigits:1];
		[nf setMaximumFractionDigits:10];
		[nf setMinimumIntegerDigits:1];
		[nf setPositivePrefix:[nf plusSign]];
		[nf setNegativePrefix:[nf minusSign]];
        
		return [NSString stringWithFormat:@"%@ %@i", [nf stringFromNumber:[NSNumber numberWithDouble:_real]], [nf stringFromNumber:[NSNumber numberWithDouble:_imag]]];
	}
	NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
	[nf setMinimumFractionDigits:1];
	[nf setMaximumFractionDigits:10];
	[nf setMinimumIntegerDigits:1];
	[nf setPositivePrefix:[nf plusSign]];
	[nf setNegativePrefix:[nf minusSign]];
    
	return [nf stringFromNumber:[NSNumber numberWithDouble:_real]];
}

- (NSString *)sympyValue {
	if ([_symbol.onlyReal boolValue]) {
		if (_unit) {
			return [[NSNumber numberWithDouble:[PMRUnitConverter toBase:_unit value:_real]] stringValue];
		}
		else {
			return [[NSNumber numberWithDouble:_real] stringValue];
		}
	}
	else {
		return [NSString stringWithFormat:@"%f+%f*I", _real, _imag];
	}
}

//-(void)setUnit:(Unit *)unit
//{
//    if (!_unit) {
//        _unit=unit;
//        return;
//    }
//
//    _val=[PMRUnitConverter convertValue:_val fromUnit:_unit toUnit:unit];
//    [_value setNumberWithNumber:[NSNumber numberWithDouble:_val]];
//    _unit=unit;
//    return;
//
//
//}
@end
