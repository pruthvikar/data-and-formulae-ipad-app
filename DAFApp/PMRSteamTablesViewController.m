//
//  PMRSteamTablesViewController.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 10/02/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import "PMRSteamTablesViewController.h"
#import "PMRConnectionManager.h"
#import "RESideMenu.h"
@interface PMRSteamTablesViewController () <UIPickerViewDataSource, UIPickerViewDelegate>
@property (strong, nonatomic) IBOutlet UITextField *leftField;

@property (strong, nonatomic) IBOutlet UITextField *rightField;
@property (strong, nonatomic) IBOutlet UILabel *solution;
@property (strong, nonatomic) IBOutlet UIPickerView *picker;
@property (strong,nonatomic) NSDictionary* dict;
@property (strong, nonatomic) NSArray *data;
@property (nonatomic,strong) NSString* left;
@property (nonatomic,strong) NSString* right;
@end

@implementation PMRSteamTablesViewController
- (IBAction)calculate:(id)sender {
	NSDictionary *variables = @{ _left:[NSNumber numberWithDouble:_leftField.text.doubleValue],
		                         _right:[NSNumber numberWithDouble:_rightField.text.doubleValue] };
	[[PMRConnectionManager sharedManager] getSteamValuesFor:variables success: ^(NSDictionary *response) {

        if(![response[@"success"] boolValue]){
        _solution.text =@"Check Input Values";
        }
        else{
             NSMutableString* ans=[NSMutableString string];
            for(id key in response[@"solution"]){
            NSString* val=_dict[key];
            if (val) {
                [ans appendString:[NSString stringWithFormat:@"%@ - %@\n",val,response[@"solution"][key]]];
                
            }
        }
         
         
            _solution.text=ans;
            
        }
	}];
}



- (IBAction)showMenu:(id)sender {
	[self.sideMenuViewController presentLeftMenuViewController];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	return 6;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	return _data[row][1];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {

    NSArray *vals = [_data[row][0] componentsSeparatedByString:@","];
    _left=vals[0];
    _right=vals[1];
	_leftField.placeholder = _dict[_left];
	_rightField.placeholder = _dict[_right];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad {

	_solution.text = @"Results";
	[super viewDidLoad];
    _leftField.text = @"";
	_rightField.text = @"";

	_picker.delegate = self;
	_picker.dataSource = self;
	_data = @[@[@"T,P", @"Temperature & Pressure"],
	          @[@"P,h", @"Pressure & Enthalpy"],
	          @[@"P,s", @"Pressure & Entropy"],
	          @[@"h,s", @"Enthalpy & Entropy"],
	          @[@"T,x", @"Temperature & Dryness Fraction"],
	          @[@"P,x", @"Pressure & Dryness Fraction"], ];
    
    _dict = @{@"T": @"Temperature, K",
	          @"P": @"Pressure, MPa",
	          @"s": @"Entropy, kJ/kg·K",
	          @"h": @"Enthalpy, kJ/kg",
	          @"x": @"Dryness Fraction",
              @"rho":@"Density, kg/m³",
              @"u":@"Specific internal energy, kJ/kg",
              @"v":@"Specific volume, m³/kg",
              @"g":@"Specific Gibbs free energy, kJ/kg"};
    	NSArray *vals = [_data[0][0] componentsSeparatedByString:@","];
    _left=vals[0];
    _right=vals[1];
	_leftField.placeholder = _dict[_left];
	_rightField.placeholder = _dict[_right];
    
    
	self.navigationController.navigationBar.barTintColor = [UIColor concreteColor];
    
    
	self.navigationController.navigationBar.translucent = NO;
	self.view.backgroundColor = [UIColor cloudsColor];
    
    
    
	// Do any additional setup after loading the view.
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
	UILabel *tView = (UILabel *)view;
	if (!tView) {
		tView = [[UILabel alloc] init];
	}
	tView.minimumScaleFactor = 0.5;
	tView.textAlignment = NSTextAlignmentCenter;
	tView.textColor = [UIColor asbestosColor];
	tView.text = [self pickerView:pickerView titleForRow:row forComponent:component];
	return tView;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
