//
//  UnitCategory.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 03/06/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import "UnitCategory.h"
#import "Symbol.h"
#import "Unit.h"


@implementation UnitCategory

@dynamic isDimensionless;
@dynamic name;
@dynamic symbols;
@dynamic units;

@end
