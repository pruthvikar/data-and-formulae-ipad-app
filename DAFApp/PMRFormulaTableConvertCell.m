//
//  PMRFormulaTableConvertCell.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 04/02/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import "PMRFormulaTableConvertCell.h"

@implementation PMRFormulaTableConvertCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self) {
		// Initialization code
	}
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];
    
	// Configure the view for the selected state
}

@end
