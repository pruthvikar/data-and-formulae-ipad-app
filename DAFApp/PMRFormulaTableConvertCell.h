//
//  PMRFormulaTableConvertCell.h
//  DAFApp
//
//  Created by Pruthvikar Reddy on 04/02/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMRFormulaTableConvertCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UITextField *real;
@property (strong, nonatomic) IBOutlet UILabel *units;
@property (strong, nonatomic) IBOutlet UIButton *solve;
@property (strong, nonatomic) IBOutlet UIButton *convert;

@end
