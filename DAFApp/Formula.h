//
//  Formula.h
//  DAFApp
//
//  Created by Pruthvikar Reddy on 05/06/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Meta.h"

@class Image, Symbol;

@interface Formula : Meta

@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSNumber * hasMain;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * summary;
@property (nonatomic, retain) NSString * legal;
@property (nonatomic, retain) NSSet *images;
@property (nonatomic, retain) NSSet *symbols;
@end

@interface Formula (CoreDataGeneratedAccessors)

- (void)addImagesObject:(Image *)value;
- (void)removeImagesObject:(Image *)value;
- (void)addImages:(NSSet *)values;
- (void)removeImages:(NSSet *)values;

- (void)addSymbolsObject:(Symbol *)value;
- (void)removeSymbolsObject:(Symbol *)value;
- (void)addSymbols:(NSSet *)values;
- (void)removeSymbols:(NSSet *)values;

@end
