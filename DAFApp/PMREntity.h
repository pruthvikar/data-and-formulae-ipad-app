//
//  PMREntity.h
//  DAFApp
//
//  Created by Pruthvikar Reddy on 06/02/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PMRNumber;
@interface PMREntity : NSObject
@property (nonatomic, strong) Unit *unit;
@property (nonatomic, strong) Symbol *symbol;
@property (nonatomic, strong) PMRNumber *value;
@property (nonatomic) double real;
@property (nonatomic) double imag;
@property (nonatomic) BOOL lockedValue;
@property (nonatomic) BOOL realValue;
+ (instancetype)entity;
- (NSString *)sympyValue;
- (NSString *)displayValue;

@end
