//
//  UnitCategory.h
//  DAFApp
//
//  Created by Pruthvikar Reddy on 03/06/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Meta.h"

@class Symbol, Unit;

@interface UnitCategory : Meta

@property (nonatomic, retain) NSNumber * isDimensionless;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *symbols;
@property (nonatomic, retain) NSSet *units;
@end

@interface UnitCategory (CoreDataGeneratedAccessors)

- (void)addSymbolsObject:(Symbol *)value;
- (void)removeSymbolsObject:(Symbol *)value;
- (void)addSymbols:(NSSet *)values;
- (void)removeSymbols:(NSSet *)values;

- (void)addUnitsObject:(Unit *)value;
- (void)removeUnitsObject:(Unit *)value;
- (void)addUnits:(NSSet *)values;
- (void)removeUnits:(NSSet *)values;

@end
