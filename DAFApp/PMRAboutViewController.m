//
//  PMRAboutViewController.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 26/05/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import "PMRAboutViewController.h"
#import "UIViewController+RESideMenu.h"
#import "RESideMenu.h"
@interface PMRAboutViewController ()

@end

@implementation PMRAboutViewController
- (IBAction)showMenu:(id)sender {
    	[self.sideMenuViewController presentLeftMenuViewController];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.barTintColor = [UIColor concreteColor];
    
    
	self.navigationController.navigationBar.translucent = NO;
	self.view.backgroundColor = [UIColor cloudsColor];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
