//
//  Formula.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 05/06/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import "Formula.h"
#import "Image.h"
#import "Symbol.h"


@implementation Formula

@dynamic category;
@dynamic hasMain;
@dynamic name;
@dynamic summary;
@dynamic legal;
@dynamic images;
@dynamic symbols;

@end
