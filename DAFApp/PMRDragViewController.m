//
//  PMRDragViewController.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 09/02/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import "PMRDragViewController.h"
#import "PMRViewController.h"
#import "Image+Methods.h"
#import "Formula+methods.h"
@interface PMRDragViewController ()
@property (nonatomic, strong) NSMutableArray *views;
@property (nonatomic, strong) UIView *touchedView;
@property (nonatomic, strong) Formula *selectedFormula;

@end

@implementation PMRDragViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	if (_selectedFormula) {
		[self addViewForFormula:_selectedFormula];
		_selectedFormula = nil;
	}
}

- (void)selectedFormula:(Formula *)selectedFormula {
	_selectedFormula = selectedFormula;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	_views = [NSMutableArray array];
	//    [self addView];
    
	UIStoryboard *mystoryboard = [UIStoryboard storyboardWithName:@"iPad" bundle:nil];
    
	//    [self performSegueWithIdentifier:@"converter" sender:self];
	PMRViewController *pc  = [mystoryboard instantiateViewControllerWithIdentifier:@"formulae"];
	[pc setModalPresentationStyle:UIModalPresentationFullScreen];
	pc.mode = @"selection";
	[pc setDelegate:self];
    
	[self presentViewController:pc animated:YES completion:nil];
	// Do any additional setup after loading the view.
}

- (void)addViewForFormula:(Formula *)formula {
	float scale = 0.5;
    
    
    
	UIImage *image = [formula getImage];
	CGSize size = CGSizeMake(ceilf((image.size.width * scale)), ceilf((image.size.height * scale)));
	CGRect rect = CGRectMake(100, 100, size.width + 40, size.height + 80);
	CGRect imgRect = CGRectMake(20, 20, size.width, size.height);
	CGRect lblRect = CGRectMake(20, size.height + 10, size.width, 80);
    
	CGRect viewFrame = [[self view] bounds];
	CGPoint center = CGPointMake((viewFrame.size.width - rect.size.width) / 2, (viewFrame.size.height - rect.size.height) / 2);
	rect.origin.x = center.x;
	rect.origin.y = center.y;
	UIView *cell = [[UIView alloc] initWithFrame:rect];
	cell.backgroundColor = [UIColor whiteColor];

	UIImageView *img = [[UIImageView alloc] initWithFrame:imgRect];
	[img setImage:image];
	[cell addSubview:img];
	[cell bringSubviewToFront:img];
	UILabel *name = [[UILabel alloc] initWithFrame:lblRect];
	[name setNumberOfLines:1];
	[name setMinimumScaleFactor:0.3];
	[name setAdjustsFontSizeToFitWidth:YES];
	[cell addSubview:name];
	[cell bringSubviewToFront:name];
    
    
	[name setText:formula.name];
    
	[_views addObject:cell];
	[self.view addSubview:cell];
    
    
    
    
	UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragged:)];
    
	[cell addGestureRecognizer:pan];
    
    
	NSArray *symbols = [formula.symbols allObjects];
	for (int i = 0; i < [symbols count]; i++) {
		UIView *lineView = [self getLineCenteredAt:cell RotatedBy:i * 1.0 / [symbols count]];
        
		[self.view addSubview:lineView];
	}
	[self.view bringSubviewToFront:cell];
}

- (CGPoint)getCenter:(UIView *)view {
	CGPoint center = CGPointMake(view.frame.origin.x + view.frame.size.width / 2, view.frame.origin.y + view.frame.size.height / 2);
	return center;
}

- (UIView *)getLineCenteredAt:(UIView *)center RotatedBy:(CGFloat)rotations {
	CGPoint cent = [self getCenter:center];
	CGFloat width = 200;
	CGFloat thickness = 3;
	UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(cent.x - width / 2, cent.y - thickness / 2, width, thickness)];
	lineView.backgroundColor = [UIColor whiteColor];
	NSLog(@"%@", NSStringFromCGRect(lineView.frame));
	lineView.layer.anchorPoint = CGPointMake(0.0, 0.5);
	CGAffineTransform rotation = CGAffineTransformMakeRotation(M_PI_2 + rotations * 2 * M_PI);
    
	lineView.transform = rotation;
	//    lineView.layer.anchorPoint=CGPointMake(0.5, 0.5);
	NSLog(@"%@", NSStringFromCGRect(lineView.frame));
	return lineView;
}

- (void)dragged:(UIPanGestureRecognizer *)recognizer {
	if (recognizer.numberOfTouches != 1) {
		return;
	}
	if (recognizer.state == UIGestureRecognizerStateBegan) {
		_touchedView = nil;
		CGPoint touch = [recognizer locationInView:self.view];
		NSLog(@"began");
        
		for (UIView *v in _views) {
			if (CGRectContainsPoint(v.frame, touch)) {
				NSLog(@"%@ %@", NSStringFromCGRect(v.frame), NSStringFromCGPoint(touch));
				_touchedView = v;
				continue;
			}
		}
	}
	if (recognizer.state == UIGestureRecognizerStateChanged) {
		//                NSLog(@"changed");
		CGPoint trans = [recognizer translationInView:self.view];
		if (_touchedView) {
			CGRect oldFrame = _touchedView.frame;
			oldFrame.origin.x += trans.x;
			oldFrame.origin.y += trans.y;
			_touchedView.frame = oldFrame;
		}
		[recognizer setTranslation:CGPointZero inView:self.view];
	}
    
	if (recognizer.state == UIGestureRecognizerStateEnded) {
		NSLog(@"ended");
	}
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
