//
//  PMRFormulaViewController.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 23/10/2013.
//  Copyright (c) 2013 Pruthvikar Reddy. All rights reserved.
//

#import "PMRFormulaViewController.h"
#import "Formula.h"
#import "Image.h"
#import "Formula+methods.h"
#import "UIImage+PMRRecolor.h"
#import "PMRFormulaTableViewController.h"
#import "Image+Methods.h"
#import "UIPopoverController+FlatUI.h"
#define HEIGHT 200.0f
#define WIDTH 768.0
@interface PMRFormulaViewController ()<UIPopoverControllerDelegate>

@property (nonatomic, strong) IBOutlet UITextView *summary;
@property (strong, nonatomic) IBOutlet UIView *container;
@property (nonatomic, strong) IBOutlet UIImageView *equation;
@property (nonatomic,strong) NSArray* images;
@property (nonatomic) NSInteger currentImage;
@property (nonatomic,strong) UIPopoverController* popover;

@end

@implementation PMRFormulaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	NSString *segueName = segue.identifier;
	if ([segueName isEqualToString:@"collection"]) {
	}
	if ([segueName isEqualToString:@"Solver"]) {
		PMRFormulaTableViewController *childViewController = (PMRFormulaTableViewController *)[segue destinationViewController];
		childViewController.formula = self.formula;
        childViewController.view.backgroundColor = [UIColor cloudsColor];
	}
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"Solver"]) {
		if ([self.formula.symbols count]==0) {
            self.container.hidden=YES;
            return NO;
        }

	}
    return YES;

}




- (void)viewDidLoad {
	[super viewDidLoad];

	self.navigationItem.title = self.formula.name;
    self.summary.editable=NO;
    
	self.summary.text = self.formula.summary;
	self.view.backgroundColor = [UIColor cloudsColor];
    self.summary.backgroundColor = [UIColor cloudsColor];
    [self.summary sizeToFit];

	self.summary.textColor = [UIColor asbestosColor];
    _currentImage=0;
    _images = [[self.formula.images allObjects] sortedArrayUsingComparator:^NSComparisonResult(Image* a, Image* b) {
        if ([[a main] boolValue])
        {

            return NSOrderedAscending;
            
        }
        else if ([[b main] boolValue])
        {

            return NSOrderedDescending;
        }
        else
        {

        return [b.pk compare:a.pk];
        }
        
    }];
    self.equation.contentMode=UIViewContentModeCenter;
	UIImage *eqn = [[_images[_currentImage] getImage] colorAnImage:[UIColor whiteColor]];
        self.equation.backgroundColor = [UIColor turquoiseColor];
    
	[self.equation.layer setCornerRadius:15.0];
    
	[self.equation setClipsToBounds:NO];
	[self.equation setImage:eqn];



    if ([_images count]>1) {
        
    



    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(imageSwipedLeft:)];
    swipeLeft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.equation addGestureRecognizer:swipeLeft];
    [self.equation setUserInteractionEnabled:YES];
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(imageSwipedRight:)];
    swipeRight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.equation addGestureRecognizer:swipeRight];
    [self.equation setUserInteractionEnabled:YES];
    }
    

    if ([self.formula.legal length]>0) {


        UIButton *button = [UIButton buttonWithType:UIButtonTypeInfoLight];
        
        [button addTarget:self action:@selector(legalTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    }
    
}

-(void)legalTapped:(UIBarButtonItem*)sender
{
    if (_popover) {
        [self.popover presentPopoverFromBarButtonItem:self.navigationItem.rightBarButtonItem
                             permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        return;
    }
    UIViewController* content = [[UIViewController alloc] init];
    UIPopoverController* popover = [[UIPopoverController alloc]
                                     initWithContentViewController:content];
    popover.delegate = self;

    UITextView* tv=[[UITextView alloc]initWithFrame:CGRectMake(0.0, 0.0, 500.0, 250.0) ];
    tv.editable=NO;
    tv.text=self.formula.legal;
    tv.backgroundColor = [UIColor cloudsColor];
    [tv sizeToFit];
    
	tv.textColor = [UIColor asbestosColor];
    [content.view addSubview:tv];
    
    // Store the popover in a custom property for later use.
    self.popover = popover;
    [popover setPopoverContentSize:tv.frame.size];
    [popover configureFlatPopoverWithBackgroundColor:[UIColor midnightBlueColor] cornerRadius:3];
    [self.popover presentPopoverFromBarButtonItem:self.navigationItem.rightBarButtonItem
                                   permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

}

-(void)viewDidLayoutSubviews
{
    
    double height =MAX(self.equation.image.size.height+50.0,HEIGHT);
    NSLog(@"%@ %f",NSStringFromCGRect(self.equation.frame),height);
    self.equation.frame=CGRectMake(self.equation.frame.origin.x, self.equation.frame.origin.y, self.equation.frame.size.width, height);
    [self moveView:self.container ];
    
    NSLog(@"%@ %f",NSStringFromCGRect(self.equation.frame),height);

}

- (void)imageSwipedLeft:(UISwipeGestureRecognizer *)gestureRecognizer {

    if (gestureRecognizer.direction==UISwipeGestureRecognizerDirectionLeft) {
        [UIView animateWithDuration:0.2 animations:^{
            self.equation.center=CGPointMake(self.equation.center.x-WIDTH, self.equation.center.y);
        }completion:^(BOOL finished) {
            self.equation.center=CGPointMake(self.equation.center.x+2*WIDTH, self.equation.center.y);
            _currentImage=(_currentImage+1)%[_images count];
            self.equation.image=[[_images[_currentImage] getImage] colorAnImage:[UIColor whiteColor]];
            double height =MAX(self.equation.image.size.height+50.0,HEIGHT);
            self.equation.frame=CGRectMake(self.equation.frame.origin.x, self.equation.frame.origin.y, self.equation.frame.size.width, height);
                        [self moveView:self.container ];

            [UIView animateWithDuration:0.2 animations:^{
            self.equation.center=CGPointMake(self.equation.center.x-WIDTH, self.equation.center.y);
            }];
        }];
    }
}

- (void)imageSwipedRight:(UISwipeGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.direction==UISwipeGestureRecognizerDirectionRight) {
        [UIView animateWithDuration:0.2 animations:^{
            self.equation.center=CGPointMake(self.equation.center.x+WIDTH, self.equation.center.y);
        }completion:^(BOOL finished) {
            self.equation.center=CGPointMake(self.equation.center.x-2*WIDTH, self.equation.center.y);
            _currentImage=(_currentImage-1)%[_images count];
            self.equation.image=[[_images[_currentImage] getImage] colorAnImage:[UIColor whiteColor]];
            double height =MAX(self.equation.image.size.height+50.0,HEIGHT);


            self.equation.frame=CGRectMake(self.equation.frame.origin.x, self.equation.frame.origin.y, self.equation.frame.size.width, height);
            [self moveView:self.container ];
            [UIView animateWithDuration:0.2 animations:^{
            self.equation.center=CGPointMake(self.equation.center.x+WIDTH, self.equation.center.y);
                    NSLog(@"%@",NSStringFromCGRect(self.equation.frame));
            }];
            
        }];
    }
}

-(void)moveView:(UIView*)view
{
view.frame=CGRectMake(view.frame.origin.x, self.equation.frame.origin.y+self.equation.frame.size.height+20.0, view.frame.size.width, view.frame.size.width);
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
