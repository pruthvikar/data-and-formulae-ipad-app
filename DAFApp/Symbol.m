//
//  Symbol.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 03/06/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import "Symbol.h"
#import "Formula.h"
#import "UnitCategory.h"


@implementation Symbol

@dynamic constant;
@dynamic display;
@dynamic isConstant;
@dynamic name;
@dynamic onlyReal;
@dynamic sympy;
@dynamic equation;
@dynamic unitCategory;

@end
