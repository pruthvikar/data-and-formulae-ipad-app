//
//  TimeStamp.h
//  DAFApp
//
//  Created by Pruthvikar Reddy on 26/05/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TimeStamp : NSManagedObject

@property (nonatomic, retain) NSDate * lastModified;
@property (nonatomic, retain) NSNumber * pk;

@end
