//
//  PMRViewController.h
//  DAFApp
//
//  Created by Pruthvikar Reddy on 22/10/2013.
//  Copyright (c) 2013 Pruthvikar Reddy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMRCollectionViewController.h"
#import "RESideMenu.h"
@interface PMRViewController : UIViewController <HideKeyboardDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSString *mode;
@property (nonatomic, weak) id delegate;
@end
