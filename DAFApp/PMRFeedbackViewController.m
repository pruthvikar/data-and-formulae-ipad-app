//
//  PMRFeedbackViewController.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 08/11/2013.
//  Copyright (c) 2013 Pruthvikar Reddy. All rights reserved.
//

#import "PMRFeedbackViewController.h"

#import "UIViewController+RESideMenu.h"
#import "RESideMenu.h"
#import "AFNetworking.h"
#import "PMRConnectionManager.h"
@interface PMRFeedbackViewController ()
@end

@implementation PMRFeedbackViewController
- (IBAction)sendFeedback:(id)sender {
    [self submitFeedback:self.comments.text];
	[self dismissViewControllerAnimated:YES completion:Nil];
}

- (IBAction)showMenu:(id)sender {
    [self.sideMenuViewController presentLeftMenuViewController];
}

- (void)submitFeedback:(NSString *)feedback {
	NSLog(@"Submitting feedback:%@", feedback);
    
    [[PMRConnectionManager sharedManager] sendFeedback:feedback success:^(NSDictionary * responseObject) {
        NSLog(@"JSON: %@", responseObject);
        self.comments.text=@"";
        [[[UIAlertView alloc]
          initWithTitle: @"Thanks!"
          message: nil
          delegate: nil
          cancelButtonTitle:@"OK"
          otherButtonTitles:nil] show];
    } failure:^{
        [[[UIAlertView alloc]
          initWithTitle: @"Sending Failed!"
          message: nil
          delegate: nil
          cancelButtonTitle:@"OK"
          otherButtonTitles:nil] show];
    }];
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	_comments.clipsToBounds = YES;
	_comments.layer.cornerRadius = 10.0f;
	[_comments becomeFirstResponder];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
