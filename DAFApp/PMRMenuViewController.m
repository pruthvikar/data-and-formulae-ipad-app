//
//  PMRMenuViewController.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 13/03/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import "PMRMenuViewController.h"
#define CELLHEIGHT 100
#import "UIViewController+RESideMenu.h"
#import "UIImage+PMRRecolor.h"
@interface PMRMenuViewController ()
@property (nonatomic, strong) NSArray *titles;
@property (nonatomic, strong) NSArray *images;
@end

@implementation PMRMenuViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	self.tableView = ({
        _titles = @[@"Formulae", @"Converter", @"Steam Tables", @"Resistor", @"Feedback", @"About"];
        _images = @[@"formulae", @"converter", @"tables", @"resistor",@"feedback", @"about"];
        UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, (self.view.frame.size.height - CELLHEIGHT * [_titles count]) / 2.0f, self.view.frame.size.width, CELLHEIGHT * [_titles count]) style:UITableViewStylePlain];
        tableView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.opaque = NO;
        tableView.backgroundColor = [UIColor clearColor];
        
        tableView.backgroundView = nil;
        tableView.backgroundColor = [UIColor clearColor];
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableView.bounces = NO;
        tableView.scrollsToTop = NO;
        tableView;
    });
	[self.view addSubview:self.tableView];
}

#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	UINavigationController *navigationController = (UINavigationController *)self.sideMenuViewController.contentViewController;
	navigationController.viewControllers = @[[self.storyboard instantiateViewControllerWithIdentifier:_titles[indexPath.row]]];
	[self.sideMenuViewController hideMenuViewController];
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return CELLHEIGHT;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex {
	return [_images count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *cellIdentifier = @"Cell";
    
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
		cell.backgroundColor = [UIColor clearColor];
        
		cell.selectedBackgroundView = [[UIView alloc] init];
        
		CGRect imgFrame = CGRectMake(CELLHEIGHT, cell.frame.origin.y + cell.frame.size.height / 3, cell.frame.size.height, cell.frame.size.height);
		CGRect lblFrame = CGRectMake(CELLHEIGHT * 2, cell.frame.origin.y + cell.frame.size.height / 3, cell.frame.size.width, cell.frame.size.height);
		UIImageView *myImageView = [[UIImageView alloc] initWithFrame:imgFrame];
		UILabel *textLabel = [[UILabel alloc] initWithFrame:lblFrame];
        
		textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:21];
		textLabel.textColor = [UIColor whiteColor];
		textLabel.highlightedTextColor = [UIColor lightGrayColor];
		textLabel.numberOfLines = 0;
        
		textLabel.text = _titles[indexPath.row];
		myImageView.image = [[UIImage imageNamed:_images[indexPath.row]]colorAnImage:[UIColor whiteColor]];
		myImageView.tintColor = [UIColor whiteColor];
		[cell addSubview:myImageView];
		[cell addSubview:textLabel];
	}
    
    
    
	return cell;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
	return UIStatusBarStyleLightContent;
}

#pragma mark -
#pragma mark RESideMenu Delegate

- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController {
	NSLog(@"willShowMenuViewController");
}

- (void)sideMenu:(RESideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController {
	NSLog(@"didShowMenuViewController");
}

- (void)sideMenu:(RESideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController {
	NSLog(@"willHideMenuViewController");
}

- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController {
	NSLog(@"didHideMenuViewController");
}

@end
