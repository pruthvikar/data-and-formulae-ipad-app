//
//  PMRNumber.h
//  DAFApp
//
//  Created by Pruthvikar Reddy on 30/01/2014.
//  Copyright (c) 2014 Pruthvikar Reddy. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PMRFormulaTableCell;
@interface PMRNumber : NSNumber

- (BOOL)setNumberWithString:(NSString *)value;
- (BOOL)setNumberWithCell:(PMRFormulaTableCell *)cell;
- (void)setNumberWithNumber:(NSNumber *)value;
- (void)setNumberWithReal:(NSNumber *)real andImaginary:(NSNumber *)imaginary;
- (NSString *)getNumberAsString;
- (NSString *)getReal;
- (NSString *)getImag;
- (NSString *)stringValue;

@end
