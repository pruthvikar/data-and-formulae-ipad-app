//
//  PMRViewController.m
//  DAFApp
//
//  Created by Pruthvikar Reddy on 22/10/2013.
//  Copyright (c) 2013 Pruthvikar Reddy. All rights reserved.
//

#import "PMRViewController.h"
#import "PMRCollectionViewController.h"
#import "UIPopoverController+FlatUI.h"
#import "UITableViewCell+FlatUI.h"
#import "UnitCategory+Methods.h"
#define CELLHEIGHT 50
@interface PMRViewController () <UIPopoverControllerDelegate, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) IBOutlet UISearchBar *seachBar;
@property (strong, nonatomic) UITableViewController *tvc;
@property (nonatomic, strong) PMRCollectionViewController *collectionViewController;
@property (nonatomic, strong) UIPopoverController *pc;
@end

@implementation PMRViewController

- (IBAction)showMenu:(id)sender {
	[self.sideMenuViewController presentLeftMenuViewController];
}

- (void)hideKeyboard {
	//    NSLog(@"delegate works!");
	[_seachBar resignFirstResponder];
	return;
}

- (NSFetchedResultsController *)fetchedResultsController {
	if (_fetchedResultsController) {
		return _fetchedResultsController;
	}
	NSManagedObjectContext *moc = [(PMRAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
	NSEntityDescription *entity = [NSEntityDescription
	                               entityForName:@"Symbol" inManagedObjectContext:moc];
    
	[fetchRequest setEntity:entity];
    
    
    
	NSPredicate *constants = [NSPredicate predicateWithFormat:@"isConstant == %@", @YES];
	//    [fetchRequest setFetchBatchSize:50];
	[fetchRequest setPredicate:constants];
    
    
	// The second sort descriptor sorts the items within each section:
	NSSortDescriptor *sortName = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    
	[fetchRequest setSortDescriptors:@[sortName]];
    
	NSFetchedResultsController *theFetchedResultsController = [[NSFetchedResultsController alloc]
	                                                           initWithFetchRequest:fetchRequest
	                                                           managedObjectContext:moc
                                                               sectionNameKeyPath:nil
                                                               cacheName:nil];
    
    
    
    
	self.fetchedResultsController = theFetchedResultsController;
	_fetchedResultsController.delegate = self;
    
	return _fetchedResultsController;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self.fetchedResultsController.fetchedObjects count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return CELLHEIGHT;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
	[cell configureFlatCellWithColor:[UIColor turquoiseColor] selectedColor:[UIColor blueColor]];
	[cell setCornerRadius:5.0];
	CGRect o = cell.frame;
	cell.frame = CGRectMake(o.origin.x, o.origin.y, 500.0, o.size.height);
	Symbol *s = [_fetchedResultsController fetchedObjects][indexPath.row];
	UILabel *label = [[UILabel alloc] initWithFrame:cell.bounds];
    
	[cell addSubview:label];
	label.text = [NSString stringWithFormat:@"%@ %@ %@", s.name, [s.constant stringValue], [[s.unitCategory getBaseUnit] display]];
	label.textAlignment = NSTextAlignmentCenter;
	return cell;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	UIBarButtonItem *constants = [[UIBarButtonItem alloc]
	                              initWithTitle:@"Constants"
                                  style:UIBarButtonItemStyleBordered
                                  target:self
                                  action:@selector(showConstants:)];
    
	//    constants.tintColor=[UIColor whiteColor];
	self.navigationItem.rightBarButtonItem = constants;
	self.navigationController.navigationBar.barTintColor = [UIColor concreteColor];
	self.seachBar.barTintColor = [UIColor concreteColor];
	self.seachBar.tintColor = [UIColor whiteColor];
	self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    
	self.navigationController.navigationBar.translucent = NO;
    
	NSError *error;
	if (![[self fetchedResultsController] performFetch:&error]) {
		// Update to handle the error appropriately.
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		exit(-1);  // Fail
	}
    
	// Do any additional setup after loading the view.
}

- (void)showConstants:(UIBarButtonItem *)sender {
	if (!_pc) {
		_tvc = [[UITableViewController alloc] init];
		NSLog(@"%@", _tvc.tableView);
		_tvc.tableView.delegate = self;
		_tvc.tableView.dataSource = self;
		[_tvc.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
		_pc = [[UIPopoverController alloc] initWithContentViewController:_tvc];
		[_pc configureFlatPopoverWithBackgroundColor:[UIColor midnightBlueColor] cornerRadius:3];
		_pc.delegate = self;
		_pc.popoverContentSize = CGSizeMake(500, [self.fetchedResultsController.fetchedObjects count] * CELLHEIGHT);
	}
	if (_pc.popoverVisible) {
		[_pc dismissPopoverAnimated:YES];
	}
	else {
		[_pc presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
	}
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	NSString *segueName = segue.identifier;
	if ([segueName isEqualToString:@"collection"]) {
		PMRCollectionViewController *childViewController = (PMRCollectionViewController *)[segue destinationViewController];
		self.collectionViewController = childViewController;
		self.collectionViewController.delegate = self;
        
        
		// do something with the AlertView's subviews here...
	}
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
	[_collectionViewController searchBar:searchBar textDidChange:searchText andScopeIndex:_seachBar.selectedScopeButtonIndex];
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope {
	[_collectionViewController searchBar:searchBar textDidChange:searchBar.text andScopeIndex:selectedScope];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
	[searchBar resignFirstResponder];
}

- (void)viewWillLayoutSubviews {
	[super viewWillLayoutSubviews];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
